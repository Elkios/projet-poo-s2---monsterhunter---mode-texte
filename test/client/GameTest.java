package client;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import fr.maya.monsterhunter.client.game.Game;
import fr.maya.monsterhunter.client.network.Client;
import fr.maya.monsterhunter.client.util.Console;

public class GameTest {

	private Game g;
	
	@Before
	public void setUp() throws Exception {
		g = new Game(new Client(new Console()), true);
		g.loadMap("newmap");
	}

	@Test
	public void testGetWidth() {
		assertEquals(g.getWidth(), 21);
	}

	@Test
	public void testGetHeight() {
		assertEquals(g.getWidth(), 21);
	}

	@Test
	public void testGetNbPath() {
		assertEquals(g.getNbPath(), 167);
	}

}
