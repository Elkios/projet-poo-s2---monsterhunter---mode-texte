package fr.maya.monsterhunter.server.entity;

import fr.maya.monsterhunter.server.plateau.Bonus;
import fr.maya.monsterhunter.server.plateau.Position;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class HunterTest {

    public Hunter hunter;
    public Player p;

    @Before
    public final void init() {
        this.hunter = new Hunter();
        this.p=this.hunter;
    }

    @Test
    public void isHunter() {
        assertTrue(hunter.isHunter());
        assertTrue(p.isHunter());
    }

    @Test
    public void getDistEclair() {
        this.hunter.setDistEclair(2);
        assertEquals(2,this.hunter.getDistEclair());

    }

    @Test
    public void setDistEclair() {
        this.hunter.setDistEclair(4);
        assertEquals(4,this.hunter.getDistEclair());
    }

    @Test
    public void appliquerBonus() {
        p.bonus.add(Bonus.FLASH);
        p.setBonusActif(0);
        p.appliquerBonus();
        assertEquals(1,this.hunter.getDistEclair());
        assertEquals(0,p.bonus.size());

    }

    @Test
    public void trouveBonus() {
        this.p.setPosition(new Position(6,0));
        hunter.trouveBonus();
        assertEquals(1,p.bonus.size());
        this.p.setPosition(new Position(12,0));
        hunter.trouveBonus();
        assertEquals(2,p.bonus.size());
    }
}