package fr.maya.monsterhunter.server.entity;

import fr.maya.monsterhunter.server.plateau.Bonus;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MonsterTest {

    private Monster m;
    private Player p;

    @Before
    public final void init() {
        this.m = new Monster();
        this.p=this.m;
    }

    @Test
    public void isHunter() {
        assertFalse(m.isHunter());
        assertFalse(p.isHunter());
    }

    @Test
    public void appliquerBonus() {
        //Monstre n'a pas de bonus
    }
}