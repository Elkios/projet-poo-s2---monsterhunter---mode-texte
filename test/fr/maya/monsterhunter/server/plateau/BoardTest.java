package fr.maya.monsterhunter.server.plateau;

import fr.maya.monsterhunter.server.entity.Monster;
import javafx.geometry.Pos;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.*;

public class BoardTest {

    Board b;
    Monster m;

    @Before
    public void setUp() throws Exception {
        b = new Board(m=new Monster());
        b.prepareMap("newmap");
    }

    @Test
    public void prepareMap() {
        assertEquals(12,b.getTab().size());
    }

    @Test
    public void getTab() {
       String s ="";
        for (int i = 0; i < b.getTab().size(); i++) {
            for (int j = 0; j < b.getTab().get(i).length; j++) {
                s+=(b.getTab().get(i)[j].getLabel());
            }
            s+='\n';
        }
        assertEquals("║║║║║║║║║║║║║║║║║║║║║\n║###║###############║\n║###║###############║\n║###║║║########║║║##║\n║###############║║║#║\n║################║║#║\n║########║║║######║#║\n║########║║║########║\n║########║║║########║\n║###################║\n║###################║\n║║║║║║║║║║║║║║║║║║║║║\n",s);
    }

    @Test
    public void getTileset() {
        assertEquals(null,b.getTileset());
    }

    @Test
    public void setTileset() {
        b.setTileset("test");
        assertEquals("test",b.getTileset());
    }

    @Test
    public void getNearcases() {
        HashSet<Position> tmp=new HashSet<Position>();
        tmp.add(new Position(1,1));
        assertEquals(tmp,b.getNearCases());
    }

    @Test
    public void getNearCases() {
        HashSet<Position> tmp=new HashSet<Position>();
        tmp.add(new Position(1,1));
        assertEquals(tmp,b.getNearCases());
    }

    @Test
    public void set() {
        b.set(Box.PATH,new Position(2,2));
        assertEquals(Box.PATH,b.get(new Position(2,2)).getType());
    }

    @Test
    public void get() {
        b.set(Box.PATH,new Position(3,2));
        assertEquals(Box.PATH,b.get(new Position(3,2)).getType());
    }

    @Test
    public void discover() {
        b.set(Box.PATH,new Position(4,4));
        assertTrue(b.discover(new Position(4,4)));
    }

    @Test
    public void getWidth() {
        assertEquals(21,b.getWidth());
    }

    @Test
    public void getHeight() {
        assertEquals(12,b.getHeight());
    }

    @Test
    public void getNbPath() {
        assertEquals(167,b.getNbPath());
    }

    @Test
    public void getMonster() {
        assertEquals(m,b.getMonster());
    }
}