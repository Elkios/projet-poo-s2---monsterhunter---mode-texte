package fr.maya.monsterhunter.server.plateau;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BoxTest {

    private Box b;
    private Box b2;

    @Before
    public void setUp() throws Exception {
        b=Box.WALL;
        b2=Box.PATH;
    }

    @Test
    public void parse() {
        Box tmp = Box.parse('#');
        assertEquals(Box.PATH,tmp);
        Box tmp2 = Box.parse('║');
        assertEquals(Box.WALL,tmp2);
    }

    @Test
    public void isAccessible() {
        assertFalse(b.isAccessible());
        assertTrue(b2.isAccessible());
    }

    @Test
    public void getLabel() {
        assertEquals('║',b.getLabel());
        assertEquals('#',b2.getLabel());
    }
}