package fr.maya.monsterhunter.server.plateau;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CaseTest {

    private Case c;
    private Box b;

    @Before
    public final void init() {
        b=Box.PATH;
        this.c=new Case(b,new Position(1,1),0);
    }

    @Test
    public void getBonus() {
        c.setBonus(Bonus.FLASH);
        assertEquals(Bonus.FLASH,c.getBonus());
    }

    @Test
    public void setBonus() {
        c.setBonus(Bonus.PARALYSANT);
        assertEquals(Bonus.PARALYSANT,c.getBonus());
    }

    @Test
    public void getTexture() {
        assertEquals(0,c.getTexture());
    }

    @Test
    public void setTexture() {
        c.setTexture(1);
        assertEquals(1,c.getTexture());
    }

    @Test
    public void getType() {
        assertEquals(b,c.getType());
    }

    @Test
    public void setType() {
        c.setType(Box.WALL);
        assertEquals(Box.WALL,c.getType());
    }

    @Test
    public void getNbTour() {
        c.setNbTour(2);
        assertEquals(2,c.getNbTour());
    }

    @Test
    public void setNbTour() {
        c.setNbTour(4);
        assertEquals(4,c.getNbTour());
    }

    @Test
    public void isDiscovered() {
        assertFalse(c.isDiscovered());
    }

    @Test
    public void setDiscovered() {
        c.setDiscovered(true);
        assertTrue(c.isDiscovered());
    }

    @Test
    public void isAccessible() {
        assertTrue(c.isAccessible());
        c.setType(Box.WALL);
        assertFalse(c.isAccessible());
    }

    @Test
    public void getLabel() {
        assertEquals("#",c.getLabel());
        c.setType(Box.WALL);
        assertEquals("║",c.getLabel());
    }

    @Test
    public void getPosition() {
        assertEquals(new Position(1,1),c.getPosition());
    }
}