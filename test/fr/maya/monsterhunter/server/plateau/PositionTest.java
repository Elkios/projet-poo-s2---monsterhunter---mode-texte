package fr.maya.monsterhunter.server.plateau;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PositionTest {

public Position p;
	
	@Before
	public void setUp() throws Exception {
		p = new Position(1, 1);
	}

	@Test
	public void testPosition() {
		Position p2 = new Position(1, 2);
		assertNotEquals(p, p2);
	}

	@Test
	public void testGetX() {
		assertEquals(p.getX(), 1);
	}

	@Test
	public void testGetY() {
		assertEquals(p.getY(), 1);
	}

	@Test
	public void testSetX() {
		p.setX(5);
		assertEquals(p.getX(), 5);
	}

	@Test
	public void testSetY() {
		p.setY(5);
		assertEquals(p.getY(), 5);
	}

	@Test
	public void testToString() {
		assertEquals(p.toString(), "[1,1]");
	}

	@Test
	public void testEqualsObject() {
		Position p2 = new Position(1, 1);
		assertTrue(p.equals(p2));
	}

}
