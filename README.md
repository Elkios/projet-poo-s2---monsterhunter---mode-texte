# Monster Hunter

Welcome to the hunting ground ! Here, you are a hunter or a monster, make your choice.
That's a turn-based game where each action can make you lose or win !

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and gaming purposes.

### Prerequisites (If you have the JAR archive, you can go now to the "Running the Game" section)

You need to install a Java IDE on your computer if that's not ready yet.

[Click here](https://www.eclipse.org/downloads/)

### Installing

You have to clone the project in your workspace

```
Click Window -> Show View -> Other -> Git/Git Repositories.
Click the "Clone a Git Repository and add the clone to this view" button.
```

Then, you see a window, copy the link below in the URI field:

```
https://git-iut.univ-lille1.fr/pomierm/projet-poo-s2---monsterhunter---mode-texte.git
```

Now, you have to create a new Java Project and select your git repository as Location.

## Running the Game

You have to run the Main class from the client package.

### Choose a Game mode

You can choose between different modes:

```
Player vs Player : Choose this if you play with another person.
Player vs AI : Choose this if you want to train your skills against our AI characters. (You can also launch an AI vs AI game).
Play Online : Choose this if you want to play against another player online.
Options : You can change the language and the map.
```

### Game's Rules

You spawn on a map that contains reachable pathes and many walls.
You can move only on the lands near you.
The Monster has to explore all the pathes to win.
The Hunter has to find the Monster until he wins.
The Monster leaves footprints on the ground so the Hunter can track him.

## Built With

* JavaFX - The UI Java Framework
* [Java 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) - The 8th JDK version

## Authors

* **Mathys Pomier** - *UX and UI developer* - [GitLab](https://git-iut.univ-lille1.fr/pomierm)
* **Yanis Sadi** - *Server developer* - [GitHub](https://github.com/sadiy)
* **Yacine Messaadi** - *Game and AI developer* - [GitLab](https://git-iut.univ-lille1.fr/messaady)
* **Adrien Morel** - *JUnit Tests writter and Entities upgrader* - [GitLab](https://git-iut.univ-lille1.fr/morela)
* **Simon Saint-Michel** - *JavaDoc writter and Structure upgrader* - [GitLab](https://git-iut.univ-lille1.fr/saintmis)
