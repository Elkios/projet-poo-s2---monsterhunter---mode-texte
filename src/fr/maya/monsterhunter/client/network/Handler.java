/**
 * <h1>Handler Class</h1>
 *
 * <p>Classe traduisant les statuts de jeu en actions</p>
 *
 * @author MYYA
 * @version 0.1
 */
package fr.maya.monsterhunter.client.network;

import fr.maya.monsterhunter.client.game.Game;
import fr.maya.monsterhunter.client.util.Exceptions;

public class Handler {

    Client client;
    Game game;
		
	public Handler(Client client) {
		this.client = client;
	}

    public void action(String data) {
        DataFormat dataFormat = DataFormat.from(data);
                
        data = data.substring(5);

        switch (dataFormat) {
            case MAP :
                this.game = client.createGame();

            	game.loadMap(data);
                break;
            case GAME_START : 
            	if(game != null) game.launch();
            	
            	break;
            case MESSAGE:
                client.interaction.log(">> " + data);

                break;
            case BROADCAST:
                client.interaction.log("[G] " + data);

                break;
            case END_GAME:
                client.interaction.log(data);
                client.clientConnection.close();

                break;
            case TOUR:
                game.tour(data.equals("1"));

                break;
            case SELECT_POS:
                client.interaction.log(Exceptions.INVALID_POSITION);
                game.tour(data.equals("1"));

                break;
		default:
			break;
        }

    }

}
