/**
 * <h1>ClientConnection Class</h1>
 *
 * <p>Classe permettant la liaison de données entre client et serveur</p>
 *
 * @author MYYA
 * @version 0.1
 */
package fr.maya.monsterhunter.client.network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import fr.maya.monsterhunter.client.util.Exceptions;

public class ClientConnection extends Thread {

    private Socket socket;

    private DataInputStream dataInputStream;
    private DataOutputStream dataOutputStream;
    
    private boolean shouldRun = true;
    private Client client;

    public ClientConnection(Client client, Socket socket) {
        this.client = client;
        this.socket = socket;
    }

    public void sendData(String data) {
        try {
            dataOutputStream.writeUTF(data);
            dataOutputStream.flush();
        } catch (IOException e) {
            System.err.println(Exceptions.ERROR_DATA_TRANSMISSION);
            close();
        }
    }

    public void run() {
        try {
            dataInputStream = new DataInputStream(socket.getInputStream());
            dataOutputStream = new DataOutputStream(socket.getOutputStream());

            while(shouldRun) {
                try{
                    while (dataInputStream.available() == 0) {
                        try {
                            Thread.sleep(1);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                    client.handler.action(dataInputStream.readUTF());

                } catch (IOException e) {
                    System.err.println(Exceptions.ERROR_CONNECT_SERVER);
                    close();
                    break;
                }
            }

            close();
        } catch (IOException e) {
            e.printStackTrace();
            close();
        }
    }

    public void close() {
        try {
            dataInputStream.close();
            dataOutputStream.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
