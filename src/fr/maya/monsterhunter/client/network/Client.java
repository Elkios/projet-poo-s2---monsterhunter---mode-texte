/**
 * <h1>Client Class</h1>
 *
 * <p>Classe définissant la relation du client avec le serveur</p>
 *
 * @author MYYA
 * @version 0.1
 */
package fr.maya.monsterhunter.client.network;

import fr.maya.monsterhunter.client.Main;
import fr.maya.monsterhunter.client.game.Game;
import fr.maya.monsterhunter.client.util.Exceptions;
import fr.maya.monsterhunter.client.util.GUI;
import fr.maya.monsterhunter.client.util.Interaction;

import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    private Game game;

    public Interaction interaction;
    public ClientConnection clientConnection;
    public Handler handler;

    public String username;

    public Client(Interaction interaction) {
        this.interaction = interaction;
        this.handler = new Handler(this);
    }

    public boolean connect(String address, int port) {
        try {
            Socket socket = new Socket(address, port);
            clientConnection = new ClientConnection(this, socket);

            clientConnection.start();

            return login();
        } catch (IOException e) {
            Main.interaction.log(Exceptions.ERROR_CONNECT_SERVER);
            return false;
        }
    }

    public Game getGame() { return this.game; }

    public boolean isInGame() { return !game.equals(null); }

    @SuppressWarnings("resource")
    public boolean login() {
        Scanner scanner = new Scanner(System.in);

        if(Main.interaction.getClass().equals(GUI.class)) {
            ((GUI) Main.interaction).nextInput("Entrez votre pseudo :", "Pseudo:", this);
        }else {
            Main.interaction.nextInput("Entrez votre pseudo :");

            String username = scanner.nextLine();
            clientConnection.sendData(DataFormat.LOGIN.outputData(username));
            clientConnection.sendData(DataFormat.CLIENT_READY.outputData());

            this.username = username;
        }

        return true;
    }

    public Game createGame() {
        boolean isHunter = username.equalsIgnoreCase("toto");

        return (this.game = new Game(this, isHunter));
    }

    /*@SuppressWarnings("resource")
    public void listenForInput() {
        Scanner scanner = new Scanner(System.in);

        while (shouldRun) {
            while (!scanner.hasNextLine()) {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

            String input = scanner.nextLine();

            if(input.startsWith("/")){
                String cmd = input.substring(1).split(" ")[0];
                String[] args = input.substring(cmd.length()).split(" ");

                if(cmd.equals("help")) {
                    clientConnection.sendData(DataFormat.HELP.outputData());
                } else if(cmd.equals("mp")){
                    if(args.length > 2){
                        clientConnection.sendData(DataFormat.MESSAGE.outputData(input));
                    }else{
                        System.err.println("Erreur de syntaxe (/mp <Pseudo> <Message>)");
                    }
                } else{
                    System.err.println("Commande inconnue (/help)");
                }

            }else {
                clientConnection.sendData(DataFormat.BROADCAST.outputData(input));
            }

        }

        clientConnection.close();
    }*/
}
