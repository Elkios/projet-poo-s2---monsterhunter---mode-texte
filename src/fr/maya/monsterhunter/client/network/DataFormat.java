/**
 * <h1>DataFormat Enum</h1>
 *
 * <p>Enum des différents types de statuts</p>
 *
 * @author MYYA
 * @version 0.1
 */
package fr.maya.monsterhunter.client.network;

public enum DataFormat {

    LOGIN("/001"),
    MESSAGE("/003"),
    MAP("/005"),
    BROADCAST("/004"),
    GAME_START("/006"),
    CLIENT_READY("/007"),
    END_GAME("/008"),
    TOUR("/009"),
    SELECT_POS("/010"),
    UNKNOWN("/404");

    private String format;

    DataFormat(String format) {
        this.format = format;
    }

    public String getFormat() {
        return format;
    }

    public String outputData(){
        return format+"?";
    }

    public String outputData(String... data){
        return format+"?"+String.join(":", data);
    }

    public static DataFormat from(String data) {
        for(DataFormat dataFormat : DataFormat.values()) {
            if(data.substring(0, 4).equals(dataFormat.getFormat())){
                return dataFormat;
            }
        }

        return DataFormat.UNKNOWN;
    }

}
