package fr.maya.monsterhunter.client;

import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import fr.maya.monsterhunter.client.game.IAGame;
import fr.maya.monsterhunter.client.game.LocalGame;
import fr.maya.monsterhunter.client.graphics.board.GraphicBoard;
import fr.maya.monsterhunter.client.graphics.menu.MenuGUI;
import fr.maya.monsterhunter.client.network.Client;

import fr.maya.monsterhunter.client.util.*;
import fr.maya.monsterhunter.server.network.Server;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Main extends Application {
	
	public final static double SCREEN_WIDTH = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
	public final static double SCREEN_HEIGHT = Toolkit.getDefaultToolkit().getScreenSize().getHeight();

	public static Scene scene;
	public static Stage stage;
	public static Interaction interaction;

	public static String selectedMap;

    public static String getPath(String folder, String file) {
        return System.getProperty("java.class.path").split(";")[0]+File.separator+folder+File.separator+file;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {

    	boolean graphicMode = true;
    	try {
            selectedMap = new PropertyUtil().get("map");
        }catch(IOException e) {
    	    interaction.log(Exceptions.ERROR, e.getMessage());
        }

    	interaction = graphicMode ? new GUI(new GraphicBoard()) : new Console();
    	
    	if(graphicMode) launch();
    	else playIA(false, false);
    }

    public static String joinOnline(String host, int port) {
    	Client client = new Client(interaction);

        if(interaction.getClass().equals(GUI.class)) scene.setRoot(MenuGUI.createLobbyGUI());

        return "";
    }

    public static void hostOnline() {
        new Server(27003).open();

        joinOnline("localhost", 27003);
    }
    
    public static void playLocal() {
    	LocalGame game = new LocalGame(interaction);
    	
    	game.launch();
    }
	
    public static void playIA(boolean IAvsIA,  boolean hunterIA) {
    	IAGame game = new IAGame(interaction, IAvsIA, hunterIA);
    	
    	game.launch();
    }
    
	@Override
	public void start(Stage stage) throws Exception {			
		scene = new Scene(new MenuGUI(), SCREEN_WIDTH, SCREEN_HEIGHT);
		this.stage = stage;
		stage.setScene(scene);
		stage.setMaximized(true);
		stage.setResizable(true);
		stage.show();
	}
    
}