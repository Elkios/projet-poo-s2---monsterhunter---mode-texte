/**
 * <h1>Exceptions Enum</h1>
 *
 * <p>Enum des différentes Exceptions</p>
 *
 * @author MYYA
 * @version 0.1
 */

package fr.maya.monsterhunter.client.util;

import fr.maya.monsterhunter.client.Main;

public enum Exceptions {
    QUEST_MONSTER_NAME(false), INVALID_NAME(true), SELECTED_POSITION(false),
    END_GAME(false), ERROR(true), INVALID_POSITION(true), MONSTER_NOT_HERE(false),
    MONSTER_HAS_BEEN_THERE(false), PLAYER_VS_PLAYER(false), ERROR_CONNECT_SERVER(true),
    ERROR_DATA_TRANSMISSION(true), ENTER_COORDINATE_CONSOLE(false),
    ERROR_ENTER_COORDINATE(true), CLICK_ON_BOX(false), POSITION_SELECTED(false),

    SELECTED_TOUR(false), OPTIONS(false), PLAY_ONLINE(false), PLAY_IA(false), HOST_ONLINE(false),
    MAIN_TITLE(false), OPTIONS_TITLE(false), LANGAGES(false), MAP_CHOICE(false), LOBBY_TITLE(false),
    CHOICE_ROLE(false), PLAYER(false), MONSTER(false), HUNTER(false), READY(false), IA_TITLE(false),
    CHOICE_IA(false), IA_GAME_TYPE(false), LAUNCH_GAME(false), HUNTER_WIN(false), MONSTER_WIN(false), BACK_MENU(false);

    public boolean isError;

    /**
     * @param isError Erreur ou non
     */
    Exceptions(boolean isError){
        this.isError = isError;
    }
    
    @Override
    public String toString() {
    	return Main.interaction.properties.get(this.name());
    }
    
    public String toString(String...strings) {
		String str = Main.interaction.properties.get(this.name());
		for(String string: strings) {
			str = str.replaceFirst("%", string);
		}
		return str;
    }
}
