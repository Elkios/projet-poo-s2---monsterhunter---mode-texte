/**
 * <h1>PropertyUtil Class</h1>
 *
 * <p>Class qui définit les fonctions de traitement de fichier</p>
 *
 * @author MYYA
 * @version 0.1
 */

package fr.maya.monsterhunter.client.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyUtil {

    private InputStream inputStream;
    private Properties prop;

    public PropertyUtil() throws IOException {
        load("config.properties");
    }

    /**
     * @param file Fichier à traiter
     */
    public PropertyUtil(String file) throws IOException {
        load(file);
    }

    /**
     * @param propFileName Fichier à traiter
     * @return Les Properties du fichier
     */
    private Properties load(String propFileName) throws IOException {
         prop = new Properties();
        try {
            inputStream = getClass().getResourceAsStream("/"+propFileName);

            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            inputStream.close();
        }

        return prop;
    }

    public void set(String key, String value) {
        prop.setProperty(key, value);
    }

    /**
     * @param key Clé de la propriété à récupérer
     * @return La propriété à récupérer
     */
    public String get(String key) {
        return prop.getProperty(key);
    }

}