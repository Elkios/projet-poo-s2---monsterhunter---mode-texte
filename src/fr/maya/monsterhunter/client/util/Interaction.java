/**
 * <h1>Abstract Class Interaction</h1>
 *
 * <p>Classe Abstraite définissant les interactions</p>
 *
 * @author MYYA
 * @version 0.1
 */
package fr.maya.monsterhunter.client.util;

import java.io.IOException;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Pattern;

import fr.maya.monsterhunter.client.Main;
import fr.maya.monsterhunter.server.plateau.Case;
import fr.maya.monsterhunter.server.plateau.Position;

public abstract class Interaction {

    public PropertyUtil properties;
    public PrintStream output = System.out;

    private enum Lang {
        FR("fr"), EN("en");

        Lang(String label) {}

        public static Lang parse(String label) {
            switch (label.toLowerCase()) {
                case "fr" :
                    return FR;
                case "en":
                    return EN;
                default:
                    return FR;
            }
        }

    }

    public void load(PropertyUtil prop) throws IOException {
        Lang lang = Lang.parse(prop.get("defaultLang"));

        properties = new PropertyUtil("langs/"+lang.toString()+".properties");
    }

    public void updateLang(String lang) {
        try {
            properties = new PropertyUtil("langs/"+lang+".properties");
            properties.set("defaultLang", lang);
        }catch(IOException e) {
            Main.interaction.log(Exceptions.ERROR.toString());
        }
    }

    public abstract void log(Exceptions exception, String... strings);

    public abstract void log(String string);

    public abstract void display(List<Case[]> tab, boolean isHunter);
    
    public abstract void display(List<Case[]> tab, boolean isHunter, Position pos, HashSet<Position> nearcases, int tour);

    public abstract Position readPosition();
    
    public abstract String nextInput(String str);
    
    public abstract String nextInput(Pattern patterns);
}
