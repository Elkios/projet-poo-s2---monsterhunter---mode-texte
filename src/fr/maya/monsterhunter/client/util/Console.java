/**
 * <h1>Console Class</h1>
 *
 * <p>Classe permettant la lecture et la vérification des entrées</p>
 *
 * @author MYYA
 * @version 0.1
 */
package fr.maya.monsterhunter.client.util;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

import fr.maya.monsterhunter.server.plateau.Case;
import fr.maya.monsterhunter.server.plateau.Position;

public class Console extends Interaction{

	private Scanner sc;

	public Console () {
		try {
			load(new PropertyUtil());
		} catch (IOException e) {
			e.printStackTrace();
		}

		sc = new Scanner(System.in);
	}

	@Override
	public void log(String string) {
		System.out.println(string);
	}

	@Override
	public void log(Exceptions exception, String... strings) {
		String print = properties.get(exception.toString());
		for(String string: strings) {
			print = print.replaceFirst("%", string);
		}
		if(!exception.isError) {
			output.println(print);
		}else {
			output.println("!!! "+print);
		}
	}

	@SuppressWarnings("resource")
	@Override
	public Position readPosition() {
		output.print(Exceptions.ENTER_COORDINATE_CONSOLE);
		String s;

		try {
			s = sc.next(Pattern.compile("([A-Z]|[a-z])(([0-9][0-9])|[0-9])"));
		} catch (Exception e) {
			output.println(Exceptions.ERROR_ENTER_COORDINATE);
			return readPosition();
		}

		int y;
		if(s.charAt(0) >= 'A' && s.charAt(0) <= 'Z') y = s.charAt(0) - 65;
		else y = s.charAt(0) - 97 + 27;
		int x = Integer.parseInt(s.substring(1)) - 1;

		return new Position(x, y);
	}

	@Override
	public void display(List<Case[]> tab, boolean isHunter, Position pos, HashSet<Position> nearcases, int tour) {
		int boardWidth = 20;
		output.print("    ");
		for (int i = 0; i < boardWidth; i++) {
			output.print((char) ('A'+i));
		}
		for (int i = 0; i < boardWidth-26; i++) {
			output.print((char) ('a'+i));
		}
		output.println();
		int i = 0;
		for(Case[] list : tab) {
			if ((i + 1) < 10) output.print("0" + (i + 1) + "  ");
			else output.print((i + 1) + "  ");
			for (Case c : list) {
				if (c != null) {
					if (!isHunter) {
						if (c.getPosition().equals(pos)) output.print("M");
						else if (c.getNbTour() != 0) output.print(c.getNbTour());
						else output.print(c.getLabel());
					} else {
						if (c.isDiscovered()) output.print(c.getNbTour());
						else output.print(c.getLabel());
					}
				}
			}
			output.println();
			i++;
		}
	}
	
	@Override
	public void display(List<Case[]> tab, boolean isHunter) {
		int boardWidth = 20;
		output.print("    ");
		for (int i = 0; i < boardWidth; i++) {
			output.print((char) ('A'+i));
		}
		for (int i = 0; i < boardWidth-26; i++) {
			output.print((char) ('a'+i));
		}
		output.println();
		int i = 0;
		for(Case[] list : tab) {
			if ((i + 1) < 10) output.print("0" + (i + 1) + "  ");
			else output.print((i + 1) + "  ");
			for (Case c : list) {
				if (c != null) {
					if (!isHunter) {
						if (c.getPosition().equals(new Position(0, 1))) output.print("M");
						else if (c.getNbTour() != 0) output.print(c.getNbTour());
						else output.print(c.getLabel());
					} else {
						if (c.isDiscovered()) output.print(c.getNbTour());
						else output.print(c.getLabel());
					}
				}
			}
			output.println();
			i++;
		}
	}

	@Override
	public String nextInput(String str) {
		return nextInput(Pattern.compile("\\w+"));
	}

	@SuppressWarnings("resource")
	@Override
	public String nextInput(Pattern pattern) {
		String input;

		try {
			input = sc.next(pattern);
		} catch (Exception e) {
			log(Exceptions.INVALID_NAME);
			return nextInput("");
		}

		return input;
	}

	public boolean hasNextLine() {
		return sc.hasNextLine();
	}
}
