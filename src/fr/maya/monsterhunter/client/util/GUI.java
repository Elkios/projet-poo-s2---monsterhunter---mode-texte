/**
 * <h1>GUI Class</h1>
 *
 * <p>Classe permettant la lecture des actions de jeu sur l'IHM</p>
 *
 * @author MYYA
 * @version 0.1
 */
package fr.maya.monsterhunter.client.util;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.regex.Pattern;

import fr.maya.monsterhunter.client.Main;
import fr.maya.monsterhunter.client.graphics.board.GraphicBoard;
import fr.maya.monsterhunter.client.network.Client;
import fr.maya.monsterhunter.server.plateau.Case;
import fr.maya.monsterhunter.server.plateau.Position;
import javafx.scene.Scene;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class GUI extends Interaction{

	GraphicBoard graphicBoard;
	
	public GUI(GraphicBoard graphicBoard) {
		this.graphicBoard = graphicBoard;
		try {
			load(new PropertyUtil());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
    @Override
    public void log(String string) {
    	graphicBoard.setFooterText(string);
    }

    @Override
    public void log(Exceptions exception, String... strings) {
		String s = properties.get(exception.name());
		for(String string: strings) {
			s = s.replaceFirst("%", string);
		}
		graphicBoard.setFooterText(s);
    }

    @Override
    public void display(List<Case[]> tab, boolean isHunter) {
    }

    @Override
    public Position readPosition() {
    	graphicBoard.setSelectedPosition(null);
    	Position p = graphicBoard.getSelectedPosition();
    	while(p == null) {
    		p = graphicBoard.getSelectedPosition();
    		log(Exceptions.CLICK_ON_BOX);
    		System.out.checkError();
    	}
    	System.out.println(Exceptions.POSITION_SELECTED);
        return graphicBoard.getSelectedPosition();
    }

    @Override
    public String nextInput(String str) {
		return "Billy";
    }

	public void nextInput(String str, String content, Client client) {
		TextInputDialog dialog = new TextInputDialog("Billy");
		String input;

		dialog.setTitle("Attente de l'utilisateur..");
		dialog.setHeaderText(str);
		dialog.setContentText(content);

		Optional<String> result = dialog.showAndWait();

		client.username = "Billy";

		result.ifPresent(name -> {
			client.username = name;

			Text text = (Text) Main.stage.getScene().lookup("#playerName");
			if (text!=null) text.setText(name);
		});


	}

    @Override
    public String nextInput(Pattern pattern) {
        return null;
    }

	@Override
	public void display(List<Case[]> tab, boolean isHunter, Position pos, HashSet<Position> nearCases, int tour) {
		double textureSize = (Main.scene.getHeight() - 200) / tab.size();
		graphicBoard.setTextureSize(textureSize);
		graphicBoard.setRole(isHunter);
		graphicBoard.setHeaderText(Exceptions.SELECTED_TOUR.toString() + tour );
		int width = (int) (tab.get(0).length * textureSize);
		int height = (int) (tab.size() * textureSize);
		graphicBoard.setCanvasSize(width, height);
    	Main.scene.setRoot(graphicBoard);
		GraphicsContext gc = graphicBoard.drawOnLayer(0);
		List<Case[]> cases = tab;
		Image tileset = new Image(getClass().getResourceAsStream("/images/tileset/complet.png"));
		Image persoImage = isHunter ? new Image(getClass().getResourceAsStream("/images/tileset/chasseur.png")) : new Image(getClass().getResourceAsStream("/images/tileset/monstre.png"));
		int sx;
		int sy;
		int texture;
		int i = 0;
		int j = 0;
		/* Affichage Map ( Background ) */ 
		for (Case[] list : cases) {
			for (Case c : list) {
				texture = c.getTexture();
				sx = (texture%23)*32;
		        sy = (int) (Math.floor(texture/23)*32);
				gc.drawImage(tileset, sx, sy, 32, 32, j * textureSize, i * textureSize, textureSize, textureSize);
				j++;
			}
			j=0; i++;
		}
		
		graphicBoard.drawOnLayer(1).clearRect(0, 0, width, height);
		graphicBoard.drawOnLayer(1).drawImage(persoImage, 0, 0, 64, 64, pos.getY()*textureSize, pos.getX()*textureSize, textureSize, textureSize);
		GraphicsContext gcHoverCases = graphicBoard.drawOnLayer(2);
		gcHoverCases.clearRect(0, 0, width, height);
		for (Position p : nearCases) {
			gcHoverCases.setFill(Color.MEDIUMPURPLE);
			gcHoverCases.setGlobalAlpha(0.5);
			gcHoverCases.fillRect(p.getY()*textureSize, p.getX()*textureSize, textureSize, textureSize);
			gcHoverCases.setFill(Color.PURPLE);
			gcHoverCases.setGlobalAlpha(1);
			gcHoverCases.setLineWidth(2);
			gcHoverCases.strokeRect(p.getY()*textureSize, p.getX()*textureSize, textureSize, textureSize);
		}
	}
    
    

}