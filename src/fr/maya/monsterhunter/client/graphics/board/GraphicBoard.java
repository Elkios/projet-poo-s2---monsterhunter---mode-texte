/**
 * <h1>GraphicBoard Class</h1>
 *
 * <p>Classe de la version graphique du board</p>
 *
 * @author MYYA
 * @version 0.1
 */
package fr.maya.monsterhunter.client.graphics.board;

import fr.maya.monsterhunter.client.Main;
import fr.maya.monsterhunter.client.util.Exceptions;
import fr.maya.monsterhunter.server.plateau.Position;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class GraphicBoard extends BorderPane{

	private CanvasMultiLayers canvas;
	private Position selectedPosition;
	private Text headerLabel;
	private Text footerLabel;
	private ImageView playerRoleImage;
	private Text roleLabel;
	private double textureSize;
	
	/**
	 * Permet de créer une instance de GraphicBoards
	 */
	public GraphicBoard() {
		
		StackPane header = new StackPane();
			headerLabel = new Text();
			headerLabel.setFill(Color.BLACK);
			headerLabel.setFont(Font.font("tw Cen MT Condensed", FontWeight.BOLD, 15));
			StackPane.setAlignment(headerLabel, Pos.CENTER);
			header.getChildren().addAll(headerLabel);
			header.setPrefHeight(100);
		StackPane footer = new StackPane();
			footerLabel = new Text();
			footerLabel.setFill(Color.BLACK);
			footerLabel.setFont(Font.font("tw Cen MT Condensed", FontWeight.BOLD, 15));
			StackPane.setAlignment(footerLabel, Pos.CENTER);
			footer.getChildren().addAll(footerLabel);
			footer.setPrefHeight(100);
		VBox playerInformation = new VBox();
			playerRoleImage = new ImageView();
			roleLabel = new Text();
			roleLabel.setFill(Color.BLACK);
			roleLabel.setFont(Font.font("tw Cen MT Condensed", FontWeight.BOLD, 15));
			playerInformation.getChildren().addAll(roleLabel, playerRoleImage);
			playerInformation.setPrefWidth(200);
			playerInformation.setAlignment(Pos.BASELINE_CENTER);
		setMargin(playerInformation, new Insets(20, 10, 20, 10));
		canvas = new CanvasMultiLayers();
		canvas.setStyle("-fx-background-color: black");
		
		setTop(header);
		setCenter(canvas);
		setBottom(footer);
		setRight(playerInformation);
		
		canvas.setOnMouseMoved(e -> {
			//System.out.println("X = " +  e.getX() + " Y = " + e.getY());
			//System.out.println("Case X = " +  Math.floor(e.getX()/64) + " Y = " + Math.floor(e.getY()/64));
			GraphicsContext gcHover = drawOnLayer(3);
			Canvas canvasHover = getLayer(3);
			gcHover.clearRect(0, 0, canvasHover.getWidth(), canvasHover.getHeight());
			
			gcHover.setFill(Color.CORNFLOWERBLUE);
			gcHover.setGlobalAlpha(0.5);
			gcHover.fillRect(Math.floor(e.getX()/textureSize)*textureSize, Math.floor(e.getY()/textureSize)*textureSize, textureSize, textureSize);
			gcHover.setStroke(Color.DODGERBLUE);
			gcHover.setGlobalAlpha(1);
			gcHover.strokeRect(Math.floor(e.getX()/textureSize)*textureSize, Math.floor(e.getY()/textureSize)*textureSize, textureSize, textureSize);
		});
		
		canvas.setOnMouseClicked(e -> {
			int y = (int)Math.floor(e.getX()/textureSize);
			int x = (int)Math.floor(e.getY()/textureSize);
			setSelectedPosition(new Position(x, y));
		});
		
	}

	/**
	 * Définit la taille des textures
	 * @param d Taille des textures
	 */
	public void setTextureSize(double d) {
		textureSize = d;
	}
	
	/**
	 * Méthode déléguée pour récupérer une couche du canvas associé au GraphicBoard
	 * @param layer Position de la couche
	 * @return Une instance de la couche associé à sa position
	 */
	public Canvas getLayer(int layer) {
		return canvas.getLayer(layer);
	}

	/**
	 * Méthode déléguée permettant de récupérer le GraphicsContext d'une couche du canvas pour dessiner dessus
	 * @param layer Position de la couche
	 * @return Une instance du GraphicsContext de la couche associé à sa position
	 */
	public GraphicsContext drawOnLayer(int layer) {
		return canvas.drawOnLayer(layer);
	}

	/**
	 * Permet de définir le texte du header du GraphicBoard
	 * @param text
	 */
	public void setHeaderText(String text) { headerLabel.setText(text); }
	
	/**
	 * Permet de définir le texte du footer du GraphicBoard
	 * @param text
	 */
	public void setFooterText(String text) { footerLabel.setText(text); }
	
	/**
	 * Permet de définir le role du joueur pour afficher le bon personnage dans les informations à droite 
	 * @param isHunter
	 * permet de savoir s'il sagit du tour du chasseur ou du monstre
	 */
	public void setRole(boolean isHunter) {
		if(isHunter) {
			roleLabel.setText("• " + Exceptions.HUNTER +" •");
			playerRoleImage.setImage(new Image(getClass().getResourceAsStream("/images/tileset/chasseur.png")));
		}else {
			roleLabel.setText("• " + Exceptions.MONSTER + " •");
			playerRoleImage.setImage(new Image(getClass().getResourceAsStream("/images/tileset/monstre.png")));
		}
	}
	
	/**
	 * Permet de changer la couleur du texte du footer
	 * @param text
	 * Nouveau texte
	 * @param c
	 * Couleur du texte
	 */
	public void setFooterText(String text, Color c) { 
		footerLabel.setFill(c);
		setFooterText(text);
	}
	
	/**
	 * Permet de récupérer la position sélectionnée par le joueur actuel
	 * @return Une instance de la postion sélectionnée
	 */
	public Position getSelectedPosition() {
		return selectedPosition;
	}

	/**
	 * Permet de définir la positionn sélectionné par le joueur
	 * @param selectedPosition
	 * Nouvelle position sélectionnée
	 */
	public void setSelectedPosition(Position selectedPosition) {
		this.selectedPosition = selectedPosition;
	}
	
	/**
	 * Méthode déléguée permettant de redéfinir la taille du canvas
	 * @param width
	 * @param height
	 */
	public void setCanvasSize(int width, int height) {
		canvas.setSize(width, height);
	}
	
}