/**
 * <h1>CanvasMultiLayers Class</h1>
 *
 * <p>Classe pour la superposition de layers sur l'IHM</p>
 *
 * @author MYYA
 * @version 0.1
 */
package fr.maya.monsterhunter.client.graphics.board;

import java.util.ArrayList;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Region;

public class CanvasMultiLayers extends Region{

	private ArrayList<Canvas> canvasLayers = new ArrayList<Canvas>();
	private int canvasWidth;
	private int canvasHeight;
	
	/**
	 *  Crée un CanvasMultiLayers avec 'nbLayers' couches de taille canvasWidth*canvasHeight
	 * @param nbLayers
	 * @param canvasWidth
	 * @param canvasHeight
	 */
	public CanvasMultiLayers(int nbLayers, int canvasWidth, int canvasHeight) {
		this.canvasWidth = canvasWidth;
		this.canvasHeight = canvasHeight;
		Canvas c;
		for (int i = 0; i < nbLayers; i++) {
			c = new Canvas(canvasWidth, canvasHeight);
			c.setStyle("-fx-border-color: red;  -fx-border-width: 10;");
			canvasLayers.add(c);
			this.getChildren().add(c);	
		}
	}
	
	/**
	 * Crée un CanvasMultiLayers par défaut avec 4 couches de 1024x1024 px
	 */
	public CanvasMultiLayers() {
		this(4, 1024, 1024);
	}

	/**
	 * Permet de récupérer la largeur du canvas
	 * @return La largeur du canvas 
	 */
	public int getCanvasWidth() {
		return canvasWidth;
	}

	/**
	 * permet de récupérer la hauteur du canvas
	 * @return la hauteur du canvas
	 */
	public int getCanvasHeight() {
		return canvasHeight;
	}
	
	/**
	 * Permet de récupérer un couche du canvas
	 * @param layer
	 * La position de la couche 			
	 * @return Une instance de la couche associé à sa position
	 */
	public Canvas getLayer(int layer) {
		return canvasLayers.get(layer);
	}
	
	/**
	 * Permet de récupérer le GraphicsContext d'une couche du canvas pour dessiner dessus
	 * @param layer
	 * La position de la couche
	 * @return Une instance du GraphicsContext de la couche associé à sa position
	 */
	public GraphicsContext drawOnLayer(int layer) {
		return getLayer(layer).getGraphicsContext2D();
	}
	
	/**
	 * Permet de redéfinir la taille du canvas
	 * @param width
	 * @param height
	 */
	public void setSize(int width, int height) {
		for (Canvas canvas : canvasLayers) {
			canvas.setWidth(width);
			canvas.setHeight(height);
		}
	}
	
}
