package fr.maya.monsterhunter.client.graphics.menu;

import javafx.geometry.Pos;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class MenuSubTitle extends StackPane{

	/**
	 * Permet de créer une instance MenuSubTitle
	 * @param name
	 * Texte du sous-titre
	 * @param size
	 * Taille du texte
	 */
	public MenuSubTitle(String name, double size) {
		
		Text text = new Text(name);
		text.setFill(Color.BLACK);
		text.setFont(Font.font("tw Cen MT Condensed", FontWeight.BOLD, size));
		
		setAlignment(Pos.CENTER);
		getChildren().addAll(text); 
		setEffect(new DropShadow(0, 3, 3, Color.WHITE));
	}
	
}
