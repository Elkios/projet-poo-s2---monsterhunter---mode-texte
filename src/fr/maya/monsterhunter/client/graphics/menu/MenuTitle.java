/**
 * <h1>MenuTitle Class</h1>
 *
 * <p>Classe définissant les titres des pages de l'IHM</p>
 *
 * @author MYYA
 * @version 0.1
 */
package fr.maya.monsterhunter.client.graphics.menu;

import javafx.geometry.Pos;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class MenuTitle extends StackPane{

	/**
	 * Permet de créer une instance de MenuTitle
	 * @param name
	 * Text du titre
	 * @param width
	 * Largeur du titre
	 * @param height
	 * Hauteur du titre
	 */
	public MenuTitle(String name, double width, double height) {
		Rectangle bg = new Rectangle(width, height);
		bg.setStroke(Color.BLACK);
		bg.setStrokeWidth(6);
		bg.setFill(Color.TRANSPARENT);
		bg.setOpacity(0.8);
		
		Text text = new Text(name);
		text.setFill(Color.BLACK);
		text.setFont(Font.font("tw Cen MT Condensed", FontWeight.BOLD, 50));
		
		setAlignment(Pos.CENTER);
		getChildren().addAll(bg, text);
		setEffect(new DropShadow(0, 3, 3, Color.WHITE));
	}
	
}
