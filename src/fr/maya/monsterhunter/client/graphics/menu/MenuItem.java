/**
 * <h1>MenuItem Class</h1>
 *
 * <p>Classe définissant les boutons de l'IHM</p>
 *
 * @author MYYA
 * @version 0.1
 */
package fr.maya.monsterhunter.client.graphics.menu;

import javafx.geometry.Pos;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class MenuItem extends StackPane{
	
	/**
	 * Permet de créer un nouvelle instance de MenuItem correspondant à un bouton du menu principale
	 * @param name
	 */
	public MenuItem(String name) {
		
		Rectangle bg = new Rectangle(400, 60);
		bg.setOpacity(0.4);
		bg.setFill(Color.WHITESMOKE);
		
		Text text = new Text(name);
		text.setFill(Color.BLACK);
		text.setFont(Font.font("tw Cen MT Condensed", FontWeight.SEMI_BOLD, 35));
		
		setAlignment(Pos.CENTER);
		getChildren().addAll(bg, text);
		
		setOnMouseEntered(e -> {
			bg.setFill(Color.ALICEBLUE);
			bg.setOpacity(0.6);
			text.setEffect(new DropShadow(0, 2, 2, Color.WHITE));
		});
		setOnMouseExited(e -> {
			bg.setFill(Color.WHITESMOKE);
			bg.setOpacity(0.4);
			text.setEffect(null);
		});
		
		
	}
	
}
