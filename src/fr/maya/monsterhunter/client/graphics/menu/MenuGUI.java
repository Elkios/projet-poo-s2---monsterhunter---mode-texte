/**
 * <h1>MenuGUI Class</h1>
 *
 * <p>Classe du menu principal du jeu</p>
 *
 * @author MYYA
 * @version 0.1
 */
package fr.maya.monsterhunter.client.graphics.menu;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Optional;

import fr.maya.monsterhunter.client.Main;
import fr.maya.monsterhunter.client.util.Exceptions;
import fr.maya.monsterhunter.client.util.Interaction;
import fr.maya.monsterhunter.client.util.PropertyUtil;
import fr.maya.monsterhunter.server.entity.Monster;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextInputDialog;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Shadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import jdk.nashorn.internal.runtime.ECMAErrors;

public class MenuGUI extends Pane {

    /**
     * Permet de créer un instance de MenuGUI correspondant au menu principal
     */
    public MenuGUI() {

        InputStream is = Main.class.getResourceAsStream("/images/background1.jpg");
        Image img = new Image(is);
        ImageView imgview = new ImageView(img);
        getChildren().add(imgview);

        Pane options = createOptionsGUI();

        MenuItem optionItem = new MenuItem(Exceptions.OPTIONS.toString());
        MenuItem joinOnlineItem = new MenuItem(Exceptions.PLAY_ONLINE.toString());
        MenuItem hostOnlineItem = new MenuItem(Exceptions.HOST_ONLINE.toString());

        MenuItem jvjItem = new MenuItem(Exceptions.PLAYER_VS_PLAYER.toString());

        MenuItem jviaItem = new MenuItem(Exceptions.PLAY_IA.toString());

        MenuTitle title = new MenuTitle(Exceptions.MAIN_TITLE.toString(), 800, 100);
        MenuBox menubox = new MenuBox(
                jvjItem,
                jviaItem,
                optionItem);

        jvjItem.setOnMouseClicked(e -> {
            Main.playLocal();
        });

        jviaItem.setOnMouseClicked(e -> {
            Main.scene.setRoot(createLobbyIAGame());
        });

        optionItem.setOnMouseClicked(e -> {
            options.setVisible(true);
        });

        joinOnlineItem.setOnMouseClicked(e -> {
            TextInputDialog dialog = new TextInputDialog("Billy");

            dialog.setTitle("Attente de l'utilisateur..");
            dialog.setHeaderText("Adresse de l'hôte");
            dialog.setContentText("Adresse IP");

            Optional<String> result = dialog.showAndWait();

            result.ifPresent(address -> {
                Main.joinOnline(address, 27003);
            });
        });

        hostOnlineItem.setOnMouseClicked(e -> {
            Main.hostOnline();
        });

        getChildren().addAll(title, menubox, options);


        title.layoutXProperty().bind(widthProperty().subtract(title.widthProperty()).divide(2));
        title.setLayoutY(150);
        menubox.layoutXProperty().bind(widthProperty().subtract(title.widthProperty()).divide(2).add(200));
        menubox.layoutYProperty().bind(heightProperty().subtract(title.heightProperty()).divide(2));

        //vbox.layoutYProperty().bind(root.heightProperty().subtract(vbox.heightProperty()).divide(3));


    }

    /**
     * Permet la création du menu des Options
     *
     * @return Une instance de Pane contenant l'interface des Options
     */
    public static Pane createOptionsGUI() {

        Pane pane = new Pane();
        ImageView background = new ImageView(new Image(Main.class.getResourceAsStream("/images/background2.jpg")));

        Group groupBack = new Group();
        Rectangle backRect = new Rectangle(50, 50);
        backRect.setFill(Color.TRANSPARENT);
        ImageView backLogo = new ImageView(new Image(Main.class.getResourceAsStream("/images/backlogo.png"), 50, 50, true, true));
        backLogo.setEffect(new DropShadow(0, 1, 1, Color.WHITE));
        groupBack.getChildren().addAll(backRect, backLogo);
        groupBack.setOnMouseEntered(e -> {
            backLogo.setEffect(new Shadow(1, Color.WHITE));
        });
        groupBack.setOnMouseExited(e -> {
            backLogo.setEffect(new DropShadow(0, 1, 1, Color.WHITE));
        });
        groupBack.setOnMouseClicked(e -> {
            pane.setVisible(false);
        });

        pane.getChildren().addAll(background, groupBack);

        groupBack.setLayoutX(20);
        groupBack.setLayoutY(20);

        MenuTitle optionsTitle = new MenuTitle(Exceptions.OPTIONS_TITLE.toString(), 600, 100);

        VBox optionsItems = new VBox();
        MenuChoiceBox cbLanguages = new MenuChoiceBox(Exceptions.LANGAGES.toString(), new String[]{"Français", "English"});
        cbLanguages.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            switch (cbLanguages.getValue()) {
                case "Français" :
                    Main.interaction.updateLang("FR");
                    break;
                case "English":
                    Main.interaction.updateLang("EN");
                    break;
            }
            Main.scene.setRoot(new MenuGUI());
        });

        File mapsFolder = new File(System.getProperty("user.dir")+File.separator+"resources"+File.separator+"maps");

        MenuChoiceBox cbMaps = new MenuChoiceBox(Exceptions.MAP_CHOICE.toString(), mapsFolder.list());
        cbMaps.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            String mapName = mapsFolder.list()[cbMaps.getSelectionModel().getSelectedIndex()];

            mapName = mapName.substring(0, mapName.lastIndexOf("."));
            Main.selectedMap = mapName;
            System.out.println(mapName);

            try {
                new PropertyUtil().set("map", mapName);
            }catch(IOException e) {
                Main.interaction.log(Exceptions.ERROR, e.getMessage());
            }
        });
        optionsItems.getChildren().addAll(cbLanguages, cbMaps);
        VBox.setMargin(cbMaps, new Insets(50, 0, 50, 0));

        optionsTitle.layoutXProperty().bind(pane.widthProperty().subtract(optionsTitle.widthProperty()).divide(2));
        optionsTitle.setLayoutY(150);
        optionsItems.layoutXProperty().bind(pane.widthProperty().subtract(optionsItems.widthProperty()).divide(2));
        optionsItems.setLayoutY(350);

        pane.getChildren().addAll(optionsTitle, optionsItems);
        pane.setPrefSize(Main.SCREEN_WIDTH, Main.SCREEN_HEIGHT);
        pane.setVisible(false);

        return pane;

    }

    /**
     * Permet la création de l'interface du lobby
     *
     * @return Une instance de Pane contenant l'interface du lobby
     */
    public static Pane createLobbyGUI() {

        Pane pane = new Pane();
        ImageView background = new ImageView(new Image(Main.class.getResourceAsStream("/images/background3.jpg")));

        Group groupBack = new Group();
        Rectangle backRect = new Rectangle(50, 50);
        backRect.setFill(Color.TRANSPARENT);
        ImageView backLogo = new ImageView(new Image(Main.class.getResourceAsStream("/images/backlogo.png"), 50, 50, true, true));
        backLogo.setEffect(new DropShadow(0, 1, 1, Color.WHITE));
        groupBack.getChildren().addAll(backRect, backLogo);
        groupBack.setOnMouseEntered(e -> {
            backLogo.setEffect(new Shadow(1, Color.WHITE));
        });
        groupBack.setOnMouseExited(e -> {
            backLogo.setEffect(new DropShadow(0, 1, 1, Color.WHITE));
        });
        groupBack.setOnMouseClicked(e -> {
            Main.scene.setRoot(new MenuGUI());
        });

        pane.getChildren().addAll(background, groupBack);

        groupBack.setLayoutX(20);
        groupBack.setLayoutY(20);

        MenuTitle lobbyTitle = new MenuTitle(Exceptions.LOBBY_TITLE.toString(), 600, 100);
        lobbyTitle.layoutXProperty().bind(pane.widthProperty().subtract(lobbyTitle.widthProperty()).divide(2));
        lobbyTitle.setLayoutY(150);
        MenuSubTitle lobbySubTitle = new MenuSubTitle(Exceptions.CHOICE_ROLE.toString(), 30);
        lobbySubTitle.layoutXProperty().bind(pane.widthProperty().subtract(lobbySubTitle.widthProperty()).divide(2));
        lobbySubTitle.setLayoutY(300);


        HBox player1 = new HBox();
        Text player1Label = new Text(Exceptions.PLAYER.toString() + " 1 : ");
        player1Label.setId("playerName");
        player1Label.setFill(Color.GREEN);
        player1Label.setFont(Font.font("tw Cen MT Condensed", FontWeight.BOLD, 15));
        ChoiceBox<String> player1cb = new ChoiceBox<String>();
        player1cb.getItems().addAll(Exceptions.MONSTER.toString(), Exceptions.HUNTER.toString());
        player1cb.setValue(Exceptions.MONSTER.toString());
        Text player1ReadyLabel = new Text(Exceptions.READY.toString());
        player1ReadyLabel.setFill(Color.BLACK);
        player1ReadyLabel.setFont(Font.font("tw Cen MT Condensed", FontWeight.BOLD, 15));
        CheckBox player1checkboxReady = new CheckBox();
        player1.getChildren().addAll(player1Label, player1cb, player1ReadyLabel, player1checkboxReady);
        player1.layoutXProperty().bind(pane.widthProperty().subtract(player1.widthProperty()).divide(2));
        player1.setLayoutY(400);
        HBox.setMargin(player1Label, new Insets(10, 10, 10, 10));
        HBox.setMargin(player1checkboxReady, new Insets(10, 10, 10, 10));
        player1.setAlignment(Pos.CENTER);

        HBox player2 = new HBox();
        Text player2Label = new Text(Exceptions.PLAYER.toString() + " 2 : ");
        player2Label.setFill(Color.BLACK);
        player2Label.setFont(Font.font("tw Cen MT Condensed", FontWeight.BOLD, 15));
        ChoiceBox<String> player2cb = new ChoiceBox<String>();
        player2cb.getItems().addAll(Exceptions.MONSTER.toString(), Exceptions.HUNTER.toString());
        player2cb.setValue(Exceptions.HUNTER.toString());
        Text player2ReadyLabel = new Text(Exceptions.READY.toString());
        player2ReadyLabel.setFill(Color.BLACK);
        player2ReadyLabel.setFont(Font.font("tw Cen MT Condensed", FontWeight.BOLD, 15));
        CheckBox player2checkboxReady = new CheckBox();
        player2.getChildren().addAll(player2Label, player2cb, player2ReadyLabel, player2checkboxReady);
        player2.layoutXProperty().bind(pane.widthProperty().subtract(player2.widthProperty()).divide(2));
        player2.setLayoutY(450);
        HBox.setMargin(player2Label, new Insets(10, 10, 10, 10));
        HBox.setMargin(player2checkboxReady, new Insets(10, 10, 10, 10));
        player2.setAlignment(Pos.CENTER);

        player2.setVisible(false);

        /* Lobby Checking */

        player1checkboxReady.selectedProperty().addListener((observable, oldvalue, newvalue) -> {
            if (newvalue) {
                if (player1cb.getValue().equals(player2cb.getValue())) {
                    player1checkboxReady.setSelected(false);
                } else {
                    player1cb.setDisable(true);
                    /* Launch game */
                }
            } else {
                player1cb.setDisable(false);
            }
        });

        player2checkboxReady.selectedProperty().addListener((observable, oldvalue, newvalue) -> {
            if (newvalue) {
                if (player2cb.getValue().equals(player1cb.getValue())) {
                    player2checkboxReady.setSelected(false);
                } else {
                    player2cb.setDisable(true);
                    /* Launch game */
                }
            } else {
                player2cb.setDisable(false);
            }
        });


        pane.setPrefSize(Main.SCREEN_WIDTH, Main.SCREEN_HEIGHT);
        pane.getChildren().addAll(lobbyTitle, lobbySubTitle, player1, player2);

        return pane;

    }

    /**
     * Permet la création du Lobby IA Game
     *
     * @return Une instance de Pane contenant l'interface du lobby IA Game
     */
    public static Pane createLobbyIAGame() {

        Pane pane = new Pane();
        ImageView background = new ImageView(new Image(Main.class.getResourceAsStream("/images/background3.jpg")));

        MenuTitle lobbyTitle = new MenuTitle(Exceptions.IA_TITLE.toString(), 600, 100);
        lobbyTitle.layoutXProperty().bind(pane.widthProperty().subtract(lobbyTitle.widthProperty()).divide(2));
        lobbyTitle.setLayoutY(150);
        MenuSubTitle lobbySubTitle = new MenuSubTitle(Exceptions.CHOICE_IA.toString(), 30);
        lobbySubTitle.layoutXProperty().bind(pane.widthProperty().subtract(lobbySubTitle.widthProperty()).divide(2));
        lobbySubTitle.setLayoutY(300);


        HBox gameType = new HBox();
        Text gameTypeLabel = new Text(Exceptions.IA_GAME_TYPE.toString());
        gameTypeLabel.setFill(Color.BLACK);
        gameTypeLabel.setFont(Font.font("tw Cen MT Condensed", FontWeight.BOLD, 15));
        ChoiceBox<String> gameTypecb = new ChoiceBox<String>();
        gameTypecb.getItems().addAll(Exceptions.MONSTER.toString() + " VS IA", "IA VS " + Exceptions.HUNTER.toString(), "IA VS IA");
        gameTypecb.setValue(Exceptions.MONSTER.toString() + " VS IA");
        gameType.getChildren().addAll(gameTypeLabel, gameTypecb);
        gameType.layoutXProperty().bind(pane.widthProperty().subtract(gameType.widthProperty()).divide(2));
        gameType.setLayoutY(400);
        HBox.setMargin(gameTypeLabel, new Insets(10, 10, 10, 10));
        gameType.setAlignment(Pos.CENTER);

        MenuItem miLaunchGame = new MenuItem(Exceptions.LAUNCH_GAME.toString());
        MenuBox mbLaunchGame = new MenuBox(miLaunchGame);
        mbLaunchGame.layoutXProperty().bind(pane.widthProperty().subtract(mbLaunchGame.widthProperty()).divide(2));
        mbLaunchGame.setLayoutY(500);

        miLaunchGame.setOnMouseClicked(e -> {
            switch (gameTypecb.getSelectionModel().getSelectedIndex()) {
                case 0 :
                    Main.playIA(false, true);
                    break;
                case 1:
                    Main.playIA(false, false);
                    break;
                case 2:
                    Main.playIA(true, false);
                    break;
            }
        });

        pane.setPrefSize(Main.SCREEN_WIDTH, Main.SCREEN_HEIGHT);
        pane.getChildren().addAll(background, lobbyTitle, lobbySubTitle, gameType, mbLaunchGame);

        return pane;
    }

    /**
     * Permet la création de l'interface de fin de jeu
     *
     * @return Une instance de Pane contenant l'interface de fin de jeu
     */
    public static Pane createEndGame(boolean isHunter) {

        StackPane pane = new StackPane();
        Text text = new Text(isHunter ? Exceptions.HUNTER_WIN.toString() : Exceptions.HUNTER_WIN.toString());
        Button button = new Button(Exceptions.BACK_MENU.toString());
        button.setTranslateY(50);
        text.setFill(Color.RED);
        text.setFont(Font.font("tw Cen MT Condensed", FontWeight.BOLD, 50));
        pane.setAlignment(Pos.CENTER);
        pane.getChildren().addAll(text, button);

        button.setOnMouseClicked(e -> {
            Main.scene.setRoot(new MenuGUI());
        });

        return pane;
    }

}
 