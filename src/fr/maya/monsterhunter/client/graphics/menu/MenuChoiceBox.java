/**
 * <h1>MenuChoiceBox Class</h1>
 *
 * <p>Classe des choiceBox custom</p>
 *
 * @author MYYA
 * @version 0.1
 */
package fr.maya.monsterhunter.client.graphics.menu;

import javafx.geometry.Insets;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.SelectionModel;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class MenuChoiceBox extends HBox{

	private Text text;
	private ChoiceBox<String> cb;
	
	/**
	 * Permet de créer une nouvelle instance de MenuChoiceBox
	 * @param label Label associé à la choicebox
	 * @param list Liste des choix de la choicebox
	 */
	public MenuChoiceBox(String label, String[] list) {
		
		text = new Text(label + " : ");
		text.setFill(Color.WHITE);
		text.setFont(Font.font("tw Cen MT Condensed", FontWeight.SEMI_BOLD, 35));
		
		cb = new ChoiceBox<String>();
			for (int i = 0; i < list.length; i++) {
				cb.getItems().add(list[i].contains(".") ? list[i].substring(0, list[i].lastIndexOf('.')) : list[i]);
			}
		cb.setPadding(new Insets(5, 10, 5, 10));
		setMargin(cb, new Insets(0, 10, 0, 10));
		
		getChildren().addAll(text, cb);
		
	}
	
	/**
	 * Permet de récupérer la valeur sélectionnée dans la choicebox
	 * @return Une instance de String contenant la valeur de la choiceb
	 */
	public String getValue() {
		return cb.getValue();
	}
	
	/**
	 * Permet de récupérer le SelectionModel de la choicebox
	 * @return Une instance de SelectionModel associée à la choicebox
	 */
	public SelectionModel<String> getSelectionModel(){
		return cb.getSelectionModel();
	}
	
	
	
}
