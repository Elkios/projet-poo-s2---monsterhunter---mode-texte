/**
 * <h1>MenuBox Class</h1>
 *
 * <p>Classe définissant les Box dans le menu de l'IHM</p>
 *
 * @author MYYA
 * @version 0.1
 */
package fr.maya.monsterhunter.client.graphics.menu;

import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

public class MenuBox extends VBox{

	/**
	 * Permet de créer une ligne séparatrices ( utilisés pour le design du menu )
	 * @return Sépérateur
	 */
	private Line createSeparator() {
		Line separator = new Line();
		separator.setEndX(400);
		separator.setStrokeWidth(2);
		separator.setStroke(Color.WHITE);
		return separator;
	}
	
	/**
	 * Permet de créer une nouvelle instance de MenuBox contenant les différents boutons du menu
	 * @param items Liste des objets dans le menu
	 */
	public MenuBox(MenuItem...items) {
		
		getChildren().add(createSeparator());
		for (MenuItem menuItem : items) {
			getChildren().addAll(menuItem, createSeparator());
		}
	}
	
}