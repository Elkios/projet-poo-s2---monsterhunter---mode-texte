/**
 * Projet POO semestre 2 : Monster Hunter
 *
 * <h1>LocalGame Class</h1>
 *
 * <p>Classe du scénario de jeu en local</p>
 *
 * @author MYSYA
 * @version 0.1
 */
package fr.maya.monsterhunter.client.game;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

import fr.maya.monsterhunter.client.Main;
import fr.maya.monsterhunter.client.graphics.menu.MenuGUI;
import fr.maya.monsterhunter.client.util.Exceptions;
import fr.maya.monsterhunter.client.util.Interaction;
import fr.maya.monsterhunter.server.entity.*;
import fr.maya.monsterhunter.server.game.GameState;
import fr.maya.monsterhunter.server.plateau.Box;
import fr.maya.monsterhunter.server.plateau.Case;
import fr.maya.monsterhunter.server.plateau.Position;
import sun.font.CreatedFontTracker;

public class LocalGame{

	private GameState gameState;
	private Player monster, hunter;

	private Interaction interaction;

	private List<Case[]> tab;
	private int boardWidth;
	private int boardHeight;
	private int nbPath;
	private int tour = 1;
	//private String tileset;
	private boolean hunterRound;

	/**
	 * @param interaction
	 */
	public LocalGame(Interaction interaction) {
		this.tab = new ArrayList<>();
		this.interaction = interaction;
		this.gameState = GameState.LOBBY;
		this.monster = new Monster();
		this.hunter = new Hunter();
		//this.nearcases = getNearCases();
	}

	/**
	 * Démarrer la partie locale
	 */
	public void launch() {
		loadMap(Main.selectedMap);
		monster.setPosition(new Position(2, 2));
		hunter.setPosition(new Position(4, 4));
		scenario();
	}

	/**
	 * Charger la map
	 * @param map Map à charger
	 */
	public void loadMap(String map) {
		tab = new ArrayList<>();
		boardWidth = getBoardWidthFromFile(map);
		BufferedReader reader;
		try {
			reader = new BufferedReader(new InputStreamReader(
					getClass().getClassLoader().getResourceAsStream("maps/"+map+".map")));
			String line = reader.readLine();
			int x = 0;
			while (line != null) {
				if(line.contains("tileset")) {
					//this.tileset = line.replace("tileset: ", "");
				}else {
					Case[] list = new Case[boardWidth];
					String[] casestr = line.replace("[", "").replace("]", "").split(" ");
					for (int j = 0; j < boardWidth; j++) {
						if(j < casestr.length) {
							char casetype = casestr[j].split(",")[0].charAt(0);
							int casetexture = Integer.parseInt(casestr[j].split(",")[1]);
							list[j] = new Case(Box.parse(casetype), x, j, casetexture);
						}
						if(list[j].getType().equals(Box.PATH)) nbPath++;
					}
					tab.add(list);
					x++;
				}
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			interaction.log(Exceptions.ERROR, e.getMessage());
		}
		boardHeight = tab.size();
	}

	/**
	 * Déplacer un joueur
	 * @param position Position vers laquelle veut se déplacer le joueur
	 * @param isHunter Sélection du joueur, vrai s'il est le chasseur, faux sinon
	 * @return
	 */
	public boolean move(Position position, boolean isHunter) {
		if(isHunter && getNearCases(hunter).contains(position)) {
			hunter.setPosition(position);
			discover(position);
			return true;
		}
		else if(!isHunter && getNearCases(monster).contains(position)) {
			monster.setPosition(position);
			tab.get(monster.getPosition().getX())[monster.getPosition().getY()].setNbTour(tour);
			return true;
		}
		return false;
	}

	/**
	 * Lancement du scenario
	 */
	public void scenario() {
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				gameState = GameState.INGAME;
			    hunterRound = false;
			    tour = 1;
			    tab.get(monster.getPosition().getX())[monster.getPosition().getY()].setNbTour(tour);
			    
			    while(gameState.equals(GameState.INGAME)) {
			    	if(hasWin(true)) {
		            	gameState = GameState.END;
		            	break;
		            }
			    	Position pos;
		            if(hunterRound){
		            	interaction.display(tab, true, hunter.getPosition(), getNearCases(hunter), tour);
		            	pos = interaction.readPosition();
		                interaction.log(Exceptions.SELECTED_POSITION, pos.toString());
		                while(!move(pos, hunterRound)) {
		                	interaction.display(tab, true, hunter.getPosition(), getNearCases(hunter), tour);
		                    interaction.log(Exceptions.SELECTED_POSITION, pos.toString());
		                    interaction.log(Exceptions.INVALID_POSITION);
		                    pos = interaction.readPosition();
		                }
	                    discover(new Position(pos.getX(), pos.getY()));
		            }else{
		            	interaction.display(tab, false, monster.getPosition(), getNearCases(monster), tour);
		            	pos = interaction.readPosition();
	                    interaction.log(Exceptions.SELECTED_POSITION, pos.toString());
		                while(!move(pos, hunterRound)) {
		                	interaction.display(tab, false, monster.getPosition(), getNearCases(monster), tour);
		                	interaction.log(Exceptions.SELECTED_POSITION, pos.toString());
		                	interaction.log(Exceptions.INVALID_POSITION);
		                    pos = interaction.readPosition();
		                }
		                interaction.log(Exceptions.SELECTED_POSITION, pos.toString());
		                
		            }

		            if(hunterRound) {
		            	interaction.display(tab, true, hunter.getPosition(), getNearCases(hunter), tour);
		           		tour++;
		            }else {
		            	interaction.display(tab, false, monster.getPosition(), getNearCases(monster), tour);
		            }

		            if(hasWin(hunterRound)) {
		            	gameState = GameState.END;
		            	break;
		            }
		            hunterRound = !hunterRound;

		            try{
		                TimeUnit.SECONDS.sleep(3);
		                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
		                System.out.print("\033[H\033[2J");
		                System.out.flush();
		            }catch (IOException | InterruptedException e) {
		                interaction.output.println("Failed to clear screen!");
		            }
		        }

		        end();
			}
		}).start();
	}

	/**
	 * Fin de jeu et afficjage de l'écran de fin de jeu
	 */
	public void end() {
		Main.scene.setRoot(MenuGUI.createEndGame(hunterRound));
	}

	/**
	 * Déterminer la largeur de la map grâce au fichier
	 * @param map Fichier map
	 * @return Largeur de la map
	 */
	private int getBoardWidthFromFile(String map) {
		int width = 0;
		BufferedReader reader;
		try {
			reader = new BufferedReader(new InputStreamReader(getClass().getClassLoader().getResourceAsStream("maps/"+map+".map")));
			String line = reader.readLine();
			while (line != null) {
				String[] linestr = line.split(" ");
				if(linestr.length > width) width = linestr.length;
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			System.out.println("ERR:"+e.getMessage());
		}
		return width;
	}

	/**
	 * @return Largeur du plateau de jeu
	 */
	public int getWidth() { return boardWidth; }

	/**
	 * @return Hauteur du plateau de jeu
	 */
	public int getHeight() { return boardHeight; }

	/**
	 * @return Map
	 */
	public List<Case[]> getTab() {
		return tab;
	}

	/**
	 * @return Nombre de chemins possibles non explorés sur la map
	 */
	public int getNbPath() {
		return nbPath;
	}

	/**
	 * Découvre une position
	 * @param position Position découverte
	 * @return Vrai si la position est découverte, faux sinon
	 */
	public boolean discover(Position position){
        if(!tab.get(position.getX())[position.getY()].getType().equals(Box.PATH)) return false;
        tab.get(position.getX())[position.getY()].setDiscovered(true);
        if(tab.get(position.getX())[position.getY()].getNbTour() == 0) {
        	interaction.log("Le monstre n'est pas encore passé par ici !");
        }else { interaction.log("Le monstre est passé ici il y a " + (tour - tab.get(position.getX())[position.getY()].getNbTour()) + " tour(s)"); }
        return true;
    }
	
	/**
     * @param hunterRound Tour du chasseur ou non
     * @return Victoire ou non du chasseur
     */
    private boolean hasWin(boolean hunterRound) {
	    return hunterRound ? monster.getPosition().equals(hunter.getPosition()) : tour == (getNbPath());
    }
    
    public HashSet<Position> getNearCases(Player p) {
        int[][] directions = new int[][]{{-1,-1}, {-1,0}, {-1,1},  {0,1}, {1,1},  {1,0},  {1,-1},  {0, -1}};
        HashSet<Position> cases = new HashSet<>();

        for (int[] direction : directions) {
            int cx = p.getPosition().getX() + direction[0];
            int cy = p.getPosition().getY() + direction[1];
            if(cy >=0 && cy < tab.size())
                if(cx >= 0 && cx < tab.get(cy).length){
                    Case c = tab.get(cx)[cy];

                    if(c.isAccessible()) {
                        cases.add(c.getPosition());
                    }
                }
        }

        return cases;
    }
}
