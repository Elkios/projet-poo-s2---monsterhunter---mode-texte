/**
 * Projet POO semestre 2 : Monster Hunter
 *
 * <h1>IAGame Class</h1>
 *
 * <p>Classe du scénario de jeu contre une IA</p>
 *
 * @author MYSYA
 * @version 0.1
 */
package fr.maya.monsterhunter.client.game;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import fr.maya.monsterhunter.client.Main;
import fr.maya.monsterhunter.client.graphics.menu.MenuGUI;
import fr.maya.monsterhunter.client.util.Exceptions;
import fr.maya.monsterhunter.client.util.Interaction;
import fr.maya.monsterhunter.server.entity.Hunter;
import fr.maya.monsterhunter.server.entity.Monster;
import fr.maya.monsterhunter.server.entity.Player;
import fr.maya.monsterhunter.server.game.GameState;
import fr.maya.monsterhunter.server.plateau.Box;
import fr.maya.monsterhunter.server.plateau.Case;
import fr.maya.monsterhunter.server.plateau.Position;

public class IAGame {

	private GameState gameState;
	private Player monster, hunter;

	private Interaction interaction;

	private List<Case[]> tab;
	//private HashSet<Position> nearcases;
	private int boardWidth;
	private int boardHeight;
	private int nbPath;
	private int tour = 1;
	private String tileset;
	private boolean hunterRound;
	private Case empreinte;
	private boolean hunterIA = false;
	private boolean IAvsIA = false;
	private Position pos;
	

	/**
     * @param interaction L'interaction à utiliser
     * @param IAvsIA Faire s'affronter 2 IA
     * @param hunterIA Jouer contre une IA chasseur ou non (pas d'effet si IAvsIA est vrai)
     */
	public IAGame(Interaction interaction, boolean IAvsIA,  boolean hunterIA) {
		this.tab = new ArrayList<>();
		this.interaction = interaction;
		this.gameState = GameState.LOBBY;
		this.monster = new Monster();
		this.hunter = new Hunter();
		this.hunterIA = hunterIA;
		this.IAvsIA = IAvsIA;
	}

	/**
     * Lance la partie
     */
	public void launch() {
		loadMap(Main.selectedMap);
		monster.setPosition(new Position(2, 2));
		hunter.setPosition(new Position(4, 4));
		scenario();
	}
	
	/**
	 * Charge le plateau depuis le fichier map
     * @param map Le nom de la carte à charger
     */
	public void loadMap(String map) {
		tab = new ArrayList<>();
		boardWidth = getBoardWidthFromFile(map);
		BufferedReader reader;
		try {
			reader = new BufferedReader(new InputStreamReader(
					getClass().getClassLoader().getResourceAsStream("maps/"+map+".map")));
			String line = reader.readLine();
			int x = 0;
			while (line != null) {
				if(line.contains("tileset")) {
					this.tileset = line.replace("tileset: ", "");
				}else {
					Case[] list = new Case[boardWidth];
					String[] casestr = line.replace("[", "").replace("]", "").split(" ");
					for (int j = 0; j < boardWidth; j++) {
						if(j < casestr.length) {
							char casetype = casestr[j].split(",")[0].charAt(0);
							int casetexture = Integer.parseInt(casestr[j].split(",")[1]);
							list[j] = new Case(Box.parse(casetype), x, j, casetexture);
						}
						if(list[j].getType().equals(Box.PATH)) nbPath++;
					}
					tab.add(list);
					x++;
				}
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			interaction.log(Exceptions.ERROR, e.getMessage());
		}
		boardHeight = tab.size();
	}

	/**
	 * Effectuer un déplacement
	 *
	 * @param position La position cible
	 * @param isHunter Chasseur ou non
     * @return Renvoie true si le déplacement est effectué
     */
	public boolean move(Position position, boolean isHunter) {
		if(isHunter && getNearCases(hunter).contains(position)) {
			hunter.setPosition(position);
			discover(position);
			return true;
		}
		else if(!isHunter && getNearCases(monster).contains(position)) {
			monster.setPosition(position);
			tab.get(monster.getPosition().getX())[monster.getPosition().getY()].setNbTour(tour);
			return true;
		}
		return false;
	}

	/**
     * Execute le scénario de la partie IAGame
     */
	public void scenario() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				gameState = GameState.INGAME;
			    hunterRound = false;
			    tour = 1;
			    tab.get(monster.getPosition().getX())[monster.getPosition().getY()].setNbTour(tour);

			    while(gameState.equals(GameState.INGAME)) {
			    	if(hasWin(true)) {
		            	gameState = GameState.END;
		            	break;
		            }
		            pos = new Position(0,0);

		            if(hunterRound){
		            	if(hunterIA || IAvsIA) moveHunter();
		            	else{
		            		interaction.display(tab, true, hunter.getPosition(), getNearCases(hunter), tour);
		            		pos = interaction.readPosition();
		                    interaction.log(Exceptions.SELECTED_POSITION, pos.toString());
		                    while(!move(pos, hunterRound)) {
			                	interaction.display(tab, true, hunter.getPosition(), getNearCases(hunter), tour);
			                    interaction.log(Exceptions.SELECTED_POSITION, pos.toString());
			                    interaction.log(Exceptions.INVALID_POSITION);
			                    pos = interaction.readPosition();
			                }
		                    discover(new Position(pos.getX(), pos.getY()));
		            	}
		                System.out.println(tour);
		            }else{
		            	if(!hunterIA || IAvsIA) moveMonster();
		            	else{
		            		interaction.display(tab, false, monster.getPosition(), getNearCases(monster), tour);
			            	pos = interaction.readPosition();
			                while(!move(pos, hunterRound)) {
			                	interaction.display(tab, false, monster.getPosition(), getNearCases(monster), tour);
			                    interaction.log(Exceptions.SELECTED_POSITION, pos.toString());
			                    interaction.log(Exceptions.INVALID_POSITION);
			                    pos = interaction.readPosition();
			                }
		            	}
		            }

		            if(hunterRound) {
		            	interaction.display(tab, true, hunter.getPosition(), getNearCases(hunter), tour);
		           		tour++;
		            }else {
		            	interaction.display(tab, false, monster.getPosition(), getNearCases(monster), tour);
		            }

		            if(hasWin(hunterRound)) {
		            	gameState = GameState.END;
		            	break;
		            }
		            hunterRound = !hunterRound;

		            try{
		                TimeUnit.SECONDS.sleep(3);
		                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
		                System.out.print("\033[H\033[2J");
		                System.out.flush();
		            }catch (IOException | InterruptedException e) {
		                interaction.output.println("Failed to clear screen!");
		            }
		        }

		        end();
			}
		}).start();
	}

	/**
     * Fin de la partie
     */
	public void end() {
		Main.scene.setRoot(MenuGUI.createEndGame(hunterRound));
	}

	/**
	 * @param map Le nom de la carte à utiliser
     * @return Renvoie la largeur du plateau à partir de la map
     */
	private int getBoardWidthFromFile(String map) {
		int width = 0;
		BufferedReader reader;
		try {
			reader = new BufferedReader(new InputStreamReader(getClass().getClassLoader().getResourceAsStream("maps/"+map+".map")));
			String line = reader.readLine();
			while (line != null) {
				String[] linestr = line.split(" ");
				if(linestr.length > width) width = linestr.length;
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			System.out.println("ERR:"+e.getMessage());
		}
		return width;
	}

	/**
     * @return Renvoie la largeur du plateau
     */
	public int getWidth() { return boardWidth; }
	
	/**
     * @return Renvoie la hauteur du plateau
     */
	public int getHeight() { return boardHeight; }

	/**
     * @return Renvoie la List des Case du plateau
     */
	public List<Case[]> getTab() {
		return tab;
	}

	/**
     * @return Renvoie le nombre de Chemins dans le plateau
     */
	public int getNbPath() {
		return nbPath;
	}

	/**
     * @param position La position de la case que l'on veut découvrir
     * @return True si la case est découvrable, false sinon
     */
	public boolean discover(Position position){
        if(!tab.get(position.getX())[position.getY()].getType().equals(Box.PATH)) return false;
        tab.get(position.getX())[position.getY()].setDiscovered(true);
        return true;
    }
	
	/**
     * @param hunterRound Tour du Chasseur ou non
     * @return Victoire ou non du Chasseur
     */
    private boolean hasWin(boolean hunterRound) {
	    return hunterRound ? monster.getPosition().equals(hunter.getPosition()) : tour == (getNbPath());
    }
    
    /**
     * @param p Le Player auquel on s'intéresse
     * @return Renvoie une HashSet des Positions adjacentes du Player p
     */
    public HashSet<Position> getNearCases(Player p) {
        int[][] directions = new int[][]{{-1,-1}, {-1,0}, {-1,1},  {0,1}, {1,1},  {1,0},  {1,-1},  {0, -1}};
        HashSet<Position> cases = new HashSet<>();

        for (int[] direction : directions) {
            int cx = p.getPosition().getX() + direction[0];
            int cy = p.getPosition().getY() + direction[1];
            if(cy >=0 && cy < tab.size())
                if(cx >= 0 && cx < tab.get(cy).length){
                    Case c = tab.get(cx)[cy];

                    if(c.isAccessible()) {
                        cases.add(c.getPosition());
                    }
                }
        }

        return cases;
    }
    
    /**
	 * Effectuer un mouvement du chasseur
     * @return vrai si le mouvement a réussi, faux sinon
     */
    public boolean moveHunter() {
    	Position pos = null;
		ArrayList<Position> near = new ArrayList<Position>();
		for(Position p : getNearCases(hunter)) {near.add(p);}
    	
		// Le Chasseur piste le monstre en suivant les empreintes
    	if(empreinte!=null) {
    		Case c = tab.get(hunter.getPosition().getX())[hunter.getPosition().getY()];
    		for(Position p : near) {
    			if(tab.get(p.getX())[p.getY()].getNbTour()>c.getNbTour()) c = tab.get(p.getX())[p.getY()];
    		}
        	discover(c.getPosition());
        	empreinte = c;
        	System.out.println("Empreinte trouvée !");
        	hunter.setPosition(c.getPosition());
        	return true;
        }
    	
    	// Le Chasseur essaye de trouver une empreinte en explorant le monde l'entourant
    	Random rand = new Random();
    	int x = rand.nextInt(near.size());
    	pos = near.get(x);
        discover(pos);
        if(tab.get(pos.getX())[pos.getY()].getNbTour()>0) { empreinte = tab.get(pos.getX())[pos.getY()]; System.out.println("Empreinte trouvée !");}
    	hunter.setPosition(pos);
        return true;
	}

	/**
	 * Effectuer un mouvement du monstre
	 * @return vrai si le mouvement a réussi, faux sinon
	 */
	public boolean moveMonster() {
		ArrayList<Position> near = new ArrayList<Position>();
		for(Position p : getNearCases(monster)) {near.add(p);}

    	
		// Le Monstre cherche un chemin qu'il n'a pas encore exploré
		for(Position p : near) {
			if(tab.get(p.getX())[p.getY()].getNbTour()==0) {
				pos = p;
				monster.setPosition(pos);
		    	tab.get(monster.getPosition().getX())[monster.getPosition().getY()].setNbTour(tour);
    			return true;
    		}
		}
		
		// Le Monstre parcourt les cases l'entourant
    	Random rand = new Random();
    	int x = rand.nextInt(near.size());
    	pos = near.get(x);
    	monster.setPosition(pos);
    	tab.get(monster.getPosition().getX())[monster.getPosition().getY()].setNbTour(tour);
        return true;
    }
    
}
