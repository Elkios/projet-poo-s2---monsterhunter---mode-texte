/**
 * Projet POO semestre 2 : Monster Hunter
 *
 * <h1>Game Class</h1>
 *
 * <p>Scénario de jeu (client)</p>
 *
 * @author MYSYA
 * @version 0.1
 */
package fr.maya.monsterhunter.client.game;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import fr.maya.monsterhunter.client.network.Client;
import fr.maya.monsterhunter.client.network.ClientConnection;
import fr.maya.monsterhunter.client.network.DataFormat;
import fr.maya.monsterhunter.client.util.Exceptions;
import fr.maya.monsterhunter.client.util.Interaction;
import fr.maya.monsterhunter.server.plateau.Box;
import fr.maya.monsterhunter.server.plateau.Case;
import fr.maya.monsterhunter.server.plateau.Position;

public class Game {

	private ClientConnection clientConnection;
	private Interaction interaction;

	public boolean isHunter;
	public Position lastMonsterPos;

    private List<Case[]> tab;
    //private HashSet<Position> nearcases;
    private int boardWidth;
    private int boardHeight;
    private int nbPath;
	private String tileset;

    /**
     * Créer une partie
     * @param client Joueur dans la partie 
     * @param isHunter Définit si le joueur est le chasseur
     */
	public Game(Client client, boolean isHunter) {
		this.isHunter = isHunter;
		this.interaction = client.interaction;
		this.clientConnection = client.clientConnection;
	}

	/**
	 * Démarre la partie
	 */
	public void launch() {
		interaction.display(tab, isHunter);
	}

	/**
	 * Charge la map depuis une chaîne de caractères
	 * @param map Chaîne de caractères correspondants aux cases de la map
	 */
	public void loadMap(String map) {
		tab = new ArrayList<>();
		boardWidth = getBoardWidthFromFile(map);
		BufferedReader reader;
		try {
			reader = new BufferedReader(new InputStreamReader(
					getClass().getClassLoader().getResourceAsStream("maps/"+map+".map")));
			String line = reader.readLine();
			int x = 0;
			while (line != null) {
				if(line.contains("tileset")) {
					this.tileset = line.replace("tileset: ", "");
				}else {
					Case[] list = new Case[boardWidth];
					String[] casestr = line.replace("[", "").replace("]", "").split(" ");
					for (int j = 0; j < boardWidth; j++) {
						if(j < casestr.length) {
							char casetype = casestr[j].split(",")[0].charAt(0);
							int casetexture = Integer.parseInt(casestr[j].split(",")[1]);
							list[j] = new Case(Box.parse(casetype), x, j, casetexture);
						}
						if(list[j].getType().equals(Box.PATH)) nbPath++;
					}
					tab.add(list);
					x++;
				}
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			interaction.log(Exceptions.ERROR, e.getMessage());
		}
		boardHeight = tab.size();
	}

	/**
	 * Définit le tour actuel
	 * @param hunterTour Le tour est celui du chasseur ou non
	 */
	public void tour(boolean hunterTour) {
		lastMonsterPos = new Position(0, 1);

		if(hunterTour && isHunter) {
			interaction.display(tab, true);

			Position position = interaction.readPosition();
			clientConnection.sendData(DataFormat.SELECT_POS.outputData(position.toString()));
		}else if(!hunterTour && !isHunter){
			interaction.display(tab, false);

			Position position = interaction.readPosition(               );
			clientConnection.sendData(DataFormat.SELECT_POS.outputData(position.toString()));
		}else {
			interaction.log("C'est au tour de l'autre joueur");
		}
	}
	
	/**
	 * Définit la longueur du plateau de jeu à partir d'un fichier
	 * @param map Caractères représentants le plateau
	 * @return Largeur du plateau
	 */
    private int getBoardWidthFromFile(String map) {
		int width = 0;
		BufferedReader reader;
		try {
			reader = new BufferedReader(new InputStreamReader(getClass().getClassLoader().getResourceAsStream("maps/"+map+".map")));
			String line = reader.readLine();
			while (line != null) {
				String[] linestr = line.split(" ");
				if(linestr.length > width) width = linestr.length;
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			System.out.println("ERR:"+e.getMessage());
		}
		return width;
    }
    
    /**
     * Retourne la largeur du plateau
     * @return Largeur du plateau
     */
    public int getWidth() { return boardWidth; }
    
    /**
     * Retourne la hauteur du plateau
     * @return Hauteur du plateau
     */
    public int getHeight() { return boardHeight; }
   
    /**
     * Retourne le plateau de jeu
     * @return Plateau de jeu
     */
    public List<Case[]> getTab() {
        return tab;
    }
 
    /**
     * Retourne le nombre de chemins total du plateau
     * @return Nombre de chemins total du plateau
     */
    public int getNbPath() {
        return nbPath;
    }

}
