package fr.maya.monsterhunter.server;

import fr.maya.monsterhunter.server.game.AbstractGame;
import fr.maya.monsterhunter.server.game.OnlineGame;
import fr.maya.monsterhunter.server.network.Server;

public class Main{

    public static void main(String[] args) throws InterruptedException {
        new Server(27003).open();
    }

    public static void lobby(Server server) {
        AbstractGame game = new OnlineGame(server.getUsers().get(0), server.getUsers().get(1));

        game.selectMap("newmap");
        game.launch();        
    }

}
