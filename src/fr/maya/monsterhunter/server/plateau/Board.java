/**
 * <h1>Board Class</h1>
 *
 * <p>Classse définissant le plateau de jeu</p>
 *
 * @author MYYA
 * @version 0.1
 */

package fr.maya.monsterhunter.server.plateau;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import fr.maya.monsterhunter.client.util.Exceptions;
import fr.maya.monsterhunter.server.entity.Monster;

public class Board {

    private List<Case[]> tab;
    private HashSet<Position> nearcases;
    private Monster monster;
    private int boardWidth;
    private int boardHeight;
    private int nbPath;
    public int nbTour;
    private String tileset;
    /**
     * @param monster Monstre à intégrer à la map
     */
    public Board(Monster monster) {
    	this.monster = monster;
    	monster.setPosition(new Position(0, 0));
    }

    public void prepareMap(String mapName) {
		tab = new ArrayList<>();
		boardWidth = getBoardWidthFromFile(mapName);
		BufferedReader reader;
		try {
			reader = new BufferedReader(new InputStreamReader(
					getClass().getClassLoader().getResourceAsStream("maps/"+mapName+".map")));
			String line = reader.readLine();
			int x = 0;
			while (line != null) {
				if(line.contains("tileset")) {
					this.tileset = line.replace("tileset: ", "");
				}else {
					Case[] list = new Case[boardWidth];
					String[] casestr = line.replace("[", "").replace("]", "").split(" ");
					for (int j = 0; j < boardWidth; j++) {
						if(j < casestr.length) {
							char casetype = casestr[j].split(",")[0].charAt(0);
							int casetexture = Integer.parseInt(casestr[j].split(",")[1]);
							list[j] = new Case(Box.parse(casetype), x, j, casetexture);
						}
						if(list[j].getType().equals(Box.PATH)) nbPath++;
					}
					tab.add(list);
					x++;
				}
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		boardHeight = tab.size();
    }

   /* public boolean moveMonster(Position newPos) {
        if(!nearcases.contains(newPos)) return false;

        this.get(monster.getPosition()).setNbTour(++nbTour);
        monster.setPosition(newPos);
        nearcases = getNearCases();
        return true;
    }*/
    
    public List<Case[]> getTab() { return tab;}

    public String getTileset() {
		return tileset;
	}

	public void setTileset(String tileset) {
		this.tileset = tileset;
	}

	public HashSet<Position> getNearcases() {
		return nearcases;
	}

	public HashSet<Position> getNearCases() {
        int[][] directions = new int[][]{{-1,-1}, {-1,0}, {-1,1},  {0,1}, {1,1},  {1,0},  {1,-1},  {0, -1}};
        HashSet<Position> cases = new HashSet<>();

        for (int[] direction : directions) {
            int cx = monster.getPosition().getX() + direction[0];
            int cy = monster.getPosition().getY() + direction[1];
            if(cy >=0 && cy < tab.size())
                if(cx >= 0 && cx < tab.get(cy).length){
                    Case c = tab.get(cx)[cy];

                    if(c.isAccessible()) {
                        cases.add(c.getPosition());
                    }
                }
        }

        return cases;
    }
        
    /**
     * @param type Type de la Box
     * @param position Position de la Box
     */
    public void set(Box type, Position position) {
        tab.get(position.getX())[position.getY()].setType(type);
    }
    /**
     * @param position Sélectionne la box à cette position
     * @return Box à la position demandée
     */
    public Case get(Position position) {
        return tab.get(position.getX())[position.getY()];
    }
    /**
     * @param position Position à inspecter
     * @return Retourne vrai si opération réussie, faux sinon
     */
    public boolean discover(Position position){
        if(!get(position).getType().equals(Box.PATH)) return false;
        get(position).setDiscovered(true);
        return true;
    }

    private int getBoardWidthFromFile(String map) {
		int width = 0;
		BufferedReader reader;
		try {
			reader = new BufferedReader(new InputStreamReader(getClass().getClassLoader().getResourceAsStream("maps/"+map+".map")));
			String line = reader.readLine();
			while (line != null) {
				String[] linestr = line.split(" ");
				if(linestr.length > width) width = linestr.length;
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			System.out.println("ERR:"+e.getMessage());
		}
		return width;
    }
    
    public int getWidth() { return boardWidth; }
    
    public int getHeight() { return boardHeight; }
    
    public int getNbPath() { return nbPath; }
    
    public Monster getMonster() { return monster; }
}
