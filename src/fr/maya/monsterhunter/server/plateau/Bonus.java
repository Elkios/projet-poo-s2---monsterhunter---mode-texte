/**
 * <h1>Bonus enum</h1>
 *
 * <p>Classse définissant le "type" bonus</p>
 *
 * @author MYYA
 * @version 0.1
 */


package fr.maya.monsterhunter.server.plateau;

public enum Bonus {
    FLASH("Flash"),
    PARALYSANT("Paralysant");

    private String nom;

    /**
     * @param nom Créer un bonus de nom "nom"
     */
    Bonus(String nom) {
        this.nom = nom;
    }
    
    /**
     * @return le nom du Bonus
     */
    @Override
    public String toString() {
        return nom;
    }

    
}
