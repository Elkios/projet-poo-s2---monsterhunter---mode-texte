/**
 * <h1>Position class</h1>
 *
 * <p>Classse définissant le "type" position</p>
 *
 * @author MYYA
 * @version 0.1
 */

package fr.maya.monsterhunter.server.plateau;

import java.util.Objects;

public class Position {

    private int x;
    private int y;

    /**
     * @param x X de la position
     * @param y Y de la position
     */
    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Position(String str) {
        str = str.substring(1, str.length()-1);

        this.x = Integer.parseInt(str.split(",")[0]);
        this.y = Integer.parseInt(str.split(",")[1]);
    }

    /**
     * @return Retourne le X de la position
     */
    public int getX() {
        return x;
    }

    /**
     * @return Retourne le Y de la position
     */
    public int getY() {
        return y;
    }

    /**
     * @param x  Définit le X de la position
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * @param y  Définit le Y de la position
     */
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "[" + x + "," + y + "]";
    }

    /**
     * @param o Objet à comparer
     * @return Vrai si même position , sinon faux
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return x == position.x &&
                y == position.y;
    }


    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
