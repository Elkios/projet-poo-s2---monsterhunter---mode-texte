/**
 * <h1>Case Class</h1>
 *
 * <p>Class qui définit les cases sur le plateau</p>
 *
 * @author MYYA
 * @version 0.1
 */

package fr.maya.monsterhunter.server.plateau;

public class Case {

	private Box type;
	private int nbTour;
	private boolean isDiscovered;
	private Position position;
	private Bonus bonus;
	private int texture;

	/**
     * @param type Type de la Case
     * @param position Position de la Case
     */
	public Case(Box type, Position position, int texture) {
		this.type = type;
		this.nbTour = 0; 
		this.isDiscovered = false;
		this.position = position;
		this.texture = texture;
	}
	
	public Case(Box type, Position position) {
		this(type, position, 0);
	}
	
	/**
     * @param type Type de la Case
     * @param x Coordonnée X
     * @param y Coordonnée Y
     */
	public Case(Box type, int x, int y) {
		this(type, new Position(x, y));
	}
	
	public Case(Box type, int x, int y, int texture) {
		this(type, new Position(x, y), texture);
	}

	public Bonus getBonus() {
		return bonus;
	}

	public void setBonus(Bonus bonus) {
		this.bonus = bonus;
	}

	public int getTexture() {
		return texture;
	}

	public void setTexture(int texture) {
		this.texture = texture;
	}

	/**
     * @return Le type de la Case
     */
	public Box getType() {
		return type;
	}

	/**
     * @param type Type de la Case
     */
	public void setType(Box type) {
		this.type = type;
	}

	/**
     * @return Le numéro du tour en cours
     */
	public int getNbTour() {
		return nbTour;
	}

	/**
     * @param nbTour Numéro du tour
     */
	public void setNbTour(int nbTour) {
		this.nbTour = nbTour;
	}

	/**
     * @return Vrai si Case découverte, faux sinon
     */
	public boolean isDiscovered() {
		return isDiscovered;
	}

	/**
     * @param isDiscovered Etat de la Case
     */
	public void setDiscovered(boolean isDiscovered) {
		this.isDiscovered = isDiscovered;
	}

	/**
     * @return Vrai si accessible, faux sinon
     */
	public boolean isAccessible() {
		return type.isAccessible();
	}

	/**
     * @return Le label de la Case
     */
	public String getLabel() {
		return Character.toString(type.getLabel());
	}
	
	/**
     * @return La position de la Case
     */
	public Position getPosition() {
		return position;
	}
	
	
	
}
