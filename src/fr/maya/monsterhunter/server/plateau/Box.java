/**
 * <h1>Box Enum</h1>
 *
 * <p>Enum des différents type de cases sur le plateau</p>
 *
 * @author MYYA
 * @version 0.1
 */

package fr.maya.monsterhunter.server.plateau;

public enum Box {
    WALL(false, '║'), TRAP(true, 'x'), PATH(true, '#'), VOID(false, ' ');

    private char label;
    private boolean isAccessible;
    /**
     * @param isAccessible Définit la Box en accessible ou non aux joueurs
     * @param label "forme" de la Box (║,x,#,.)
     */
    Box(boolean isAccessible, char label) {
        this.isAccessible = isAccessible;
        this.label = label;
    }
    /**
     * @param label "forme" de la Box (║,x,#,.)
     * @return Retourne le label sous forme "wall" , "trap"...
     */
    public static Box parse(char label) {
        switch (label) {
            case '║':
                return WALL;
            case 'x' :
                return TRAP;
            case '#' :
                return PATH;
            default:
                return VOID;
        }
    }
    /**
     * @return Retourne l'accessibilité de l'instance
     */
    public boolean isAccessible() {
        return isAccessible;
    }
    /**
     * @return Retourne le label de l'instance
     */
    public char getLabel() {
        return label;
    }
}
