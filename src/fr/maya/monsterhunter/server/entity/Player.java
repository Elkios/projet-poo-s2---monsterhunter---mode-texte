/**
 * <h1>Abstract Class Player</h1>
 *
 * <p>Classe Abstraite définissant un Joueur</p>
 *
 * @author MYYA
 * @version 0.1
 */
package fr.maya.monsterhunter.server.entity;

import fr.maya.monsterhunter.server.plateau.Bonus;
import fr.maya.monsterhunter.server.plateau.Position;

import java.util.ArrayList;

public abstract class Player {

	protected Player() {
		this.position = new Position(0,0);
	}
	
    private int score;

    protected Bonus bonusActif;
    
    protected ArrayList<Bonus> bonus = new ArrayList<>();
    
    private Position position;

    protected int getScore() { return score; }

    public Position getPosition() { return this.position;}

    public void setPosition(Position position) {this.position = position;}

    protected void updateScore(int score){ this.score = score; }

    public abstract void appliquerBonus();

    public abstract boolean isHunter();

    /**
     * @return Retourne le Bonus actuel du Joueur;
     */
    public Bonus getBonusActif() {
        return bonusActif;
    }

    /**
     * @param i Définit le bonus actif du chasseur
     */
    public void setBonusActif(int i) {
        this.bonusActif = bonus.get(i);
    }
}
