/**
 * <h1>Monster class</h1>
 *
 * <p>Cette classe gère toutes les caractéristiques du joueur de type Monster</p>
 *
 * @author MYYA
 * @version 0.1
 */
package fr.maya.monsterhunter.server.entity;


public class Monster extends Player {

    public Monster() {
    	super();
    }

    @Override
    public boolean isHunter() {
        return false;
    }

    @Override
    public void appliquerBonus() {
    }
}
