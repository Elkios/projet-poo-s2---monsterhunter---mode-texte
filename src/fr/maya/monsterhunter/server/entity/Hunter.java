/**
 * <h1>Hunter class</h1>
 *
 * <p>Cette classe gère toutes les caractéristiques du joueur de type Hunter</p>
 *
 * @author MYYA
 * @version 0.1
 */
package fr.maya.monsterhunter.server.entity;

import fr.maya.monsterhunter.server.plateau.Bonus;

public class Hunter extends Player {

    private int distEclair;

    public Hunter() {
        
    }

    @Override
    public boolean isHunter() {
        return true;
    }

    public int getDistEclair() {
        return distEclair;
    }

    /**
     * @param distEclair Définit la distance d'éclairage
     */
    public void setDistEclair(int distEclair) {
        this.distEclair = distEclair;
    }

    @Override
    public void appliquerBonus() {
        if (bonusActif.toString() == "Flash") this.distEclair = 1;
        bonus.remove(bonusActif);
    }

    public void trouveBonus() {
        if((getPosition().getX()+getPosition().getY())%6==0) {
            bonus.add(Bonus.FLASH);
            System.out.println("Vous avez trouvé un Bonus Flash !");
        }
        else if((getPosition().getX()+getPosition().getY())%12==0) {
            bonus.add(Bonus.PARALYSANT);
            System.out.println("Vous avez trouvé un Bonus Paralysant !");
        }
    }
}
