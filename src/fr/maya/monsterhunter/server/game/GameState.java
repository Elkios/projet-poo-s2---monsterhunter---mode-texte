/**
 * <h1>GameState Enum</h1>
 *
 * <p>Enum des différents états de jeu</p>
 *
 * @author MYYA
 * @version 0.1
 */
package fr.maya.monsterhunter.server.game;

public enum GameState {
    LOBBY ,INGAME, END;
}
