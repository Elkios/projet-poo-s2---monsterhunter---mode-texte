/**
 * <h1>Abstract Class AbstractGame</h1>
 *
 * <p>Classe Abstraite définissant les méthodes d'un scénario de jeu</p>
 *
 * @author MYYA
 * @version 0.1
 */
package fr.maya.monsterhunter.server.game;

import fr.maya.monsterhunter.server.network.User;
import fr.maya.monsterhunter.server.plateau.Board;
import fr.maya.monsterhunter.server.plateau.Position;

public abstract class AbstractGame {

    protected Board board;

    public abstract void selectMap(String mapName);

    public abstract void launch();

    protected boolean hasWin(Board board, boolean hunterRound, Position position) {
        return hunterRound ? board.getMonster().getPosition().equals(position) : board.nbTour == (board.getNbPath());
    }

    public abstract void move(User user, Position position);

    public abstract void scenario();

    public abstract void end();
}
