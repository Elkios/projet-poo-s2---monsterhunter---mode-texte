/**
 * <h1>IAGame Class</h1>
 *
 * <p>Classe du scénario de jeu avec une IA</p>
 *
 * @author MYYA
 * @version 0.1
 */
package fr.maya.monsterhunter.server.game;

import fr.maya.monsterhunter.server.entity.Hunter;
import fr.maya.monsterhunter.server.entity.Monster;
import fr.maya.monsterhunter.server.entity.Player;
import fr.maya.monsterhunter.server.network.User;
import fr.maya.monsterhunter.server.plateau.Board;
import fr.maya.monsterhunter.server.plateau.Position;

public class IAGame extends AbstractGame{

	private GameState gameState;
    private Player monster, hunter;
    private Board board;
    private boolean iaHunter;
    
	
    public IAGame(Player monster, Player hunter) {
    	if(monster.equals(null)) {
    		this.monster = new Monster();
    		this.hunter = hunter;
    	}else {
    		this.hunter = new Hunter();
    		this.monster = monster;
    		this.iaHunter = true;
    	}
    }
    
    
	@Override
	public void selectMap(String mapName) {
		
	}

	@Override
	public void launch() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void move(User user, Position position) {
		
	}

	@Override
	public void scenario() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void end() {
		// TODO Auto-generated method stub
		
	}

}
