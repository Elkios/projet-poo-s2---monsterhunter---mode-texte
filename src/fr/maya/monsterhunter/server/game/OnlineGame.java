/**
 * <h1>OnlineGame Class</h1>
 *
 * <p>Classe du scénario de jeu en ligne</p>
 *
 * @author MYYA
 * @version 0.1
 */
package fr.maya.monsterhunter.server.game;

import fr.maya.monsterhunter.client.network.DataFormat;
import fr.maya.monsterhunter.server.entity.*;
import fr.maya.monsterhunter.server.network.User;
import fr.maya.monsterhunter.server.plateau.Board;
import fr.maya.monsterhunter.server.plateau.Position;

public class OnlineGame extends AbstractGame {

    private GameState gameState;
    private User monster, hunter;
    private Board board;

    public OnlineGame(User monster, User hunter) {
        monster.setGame(this);
        monster.setPlayer(new Monster());

        hunter.setGame(this);
        hunter.setPlayer(new Hunter());

        this.gameState = GameState.LOBBY;
        this.monster = monster;
        this.hunter = hunter;
    }

    private void broadcastData(DataFormat dataFormat, String... data) {
        monster.sendData(dataFormat.outputData(data));
        hunter.sendData(dataFormat.outputData(data));
    }

    @Override
    public void selectMap(String mapName) {
        this.board = new Board((Monster) monster.getPlayer());

        broadcastData(DataFormat.MAP, mapName);
        broadcastData(DataFormat.BROADCAST, "En attente des autres joueurs...");

        System.out.println("LOBBY ["+monster.getUsername()+","+hunter.getUsername()+"]");

        board.prepareMap(mapName);

        this.gameState = GameState.LOBBY;
    }

    @Override
    public void launch() {
        broadcastData(DataFormat.GAME_START);

        System.out.println("INGAME ["+monster.getUsername()+","+hunter.getUsername()+"]");

        scenario();
    }

    @Override
    public void move(User user, Position position) {
        Player player = user.getPlayer();

        if(!player.isHunter()) {
            if (board.getNearCases().contains(position)) {
                broadcastData(DataFormat.TOUR, "1");
            } else {
                user.sendData(DataFormat.SELECT_POS.outputData("0"));
            }
        }
    }

    @Override
    public void scenario() {
        this.gameState = GameState.INGAME;

        broadcastData(DataFormat.TOUR, "0");
    }

    @Override
    public void end() {
        this.gameState = GameState.END;

        broadcastData(DataFormat.END_GAME, "Fin de la partie.");

        monster.close();
        hunter.close();
    }

    /*@Override
    public void selectMap() {
        client.clientConnection.sendData(DataFormat.QUEST_MAP.outputData());
        selectedMap = client.clientConnection.nextData();
    }

    @Override
    public void launch(String username) {
        client.login(username);
        client.clientConnection.start();

        board = new Board(selectedMap, new Monster(new Position(1, 1), 20, username));

        scenario();
    }

    @Override
    public void move(boolean isHunter, Position position) {
        client.clientConnection.sendData(DataFormat.QUEST_MOVE.outputData());

        boolean canMove = Boolean.parseBoolean(client.clientConnection.nextData());

        if(canMove && !isHunter) {
            board.moveMonster(position);
        }{
            interaction.log(Exceptions.INVALID_POSITION);
        }
    }

    @Override
    public void scenario() {
        boolean ingame = true;
        boolean hunterRound = false;
        tour = 0;

        while(ingame) {
            board.display(hunterRound);

            interaction.log(Exceptions.SELECTED_POSITION, board.getMonster().getPosition().toString());
            Position pos = interaction.readPosition(board);

            if(hunterRound && isHunter){
                while (!board.get(pos).isAccessible()) {
                    interaction.log(Exceptions.SELECTED_POSITION, pos.toString());
                    interaction.log(Exceptions.INVALID_POSITION);
                    pos = interaction.readPosition(board);
                }

                board.discover(new Position(pos.getX(), pos.getY()));
            }else{
                while(!board.moveMonster(pos)) {
                    interaction.log(Exceptions.SELECTED_POSITION, pos.toString());
                    interaction.log(Exceptions.INVALID_POSITION);
                    pos = interaction.readPosition(board);
                }
            }

            ingame = !hasWin(board, hunterRound, pos);
            hunterRound = !hunterRound;

            tour++;
        }
    }

    @Override
    public void end() {
        client.clientConnection.close();

        interaction.log(Exceptions.END_GAME, "Tours: "+tour);
    }*/
}
