/**
 * <h1>User Class</h1>
 *
 * <p>Classe définissant un utilisateur</p>
 *
 * @author MYYA
 * @version 0.1
 */
package fr.maya.monsterhunter.server.network;


import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import fr.maya.monsterhunter.server.entity.Player;
import fr.maya.monsterhunter.server.game.AbstractGame;

public class User extends Thread {

    private String username;
    private Socket socket;
    private Server server;
    private DataOutputStream dataOutputStream;
    private DataInputStream dataInputStream;
    private boolean isReady = false;

    private AbstractGame game;

    private Player player;

    public User(Socket socket, Server server) {
        super("[Server" + server.getServerId() + "]-ConnectionThread");

        this.socket = socket;
        this.server = server;
    }

    public void setPlayer(Player player) { this.player = player; }

    public Player getPlayer() { return player; }

    public void setGame(AbstractGame game) { this.game = game; }

    public AbstractGame getGame() { return game; }

    public String getUsername() {
        return username;
    }

    public boolean isReady() {
        return this.isReady;
    }

    public void updateReady() {
        this.isReady = !isReady;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void sendData(String data) {
        try {
            dataOutputStream.writeUTF(data);
            dataOutputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Server getServer() {
        return server;
    }

    public void run() {
        boolean shouldRun = true;

        try {
            dataInputStream = new DataInputStream(socket.getInputStream());
            dataOutputStream = new DataOutputStream(socket.getOutputStream());

            Handler handler = new Handler(this);

            while (shouldRun) {
                while (dataInputStream.available() == 0) {
                    Thread.sleep(1);
                }

                String data = dataInputStream.readUTF();

                handler.action(data);
            }

            close();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            dataInputStream.close();
            dataOutputStream.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
