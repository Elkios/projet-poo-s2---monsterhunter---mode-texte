/**
 * <h1>Server Class</h1>
 *
 * <p>Classe démarrant le serveur de jeu</p>
 *
 * @author MYYA
 * @version 0.1
 */
package fr.maya.monsterhunter.server.network;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {

    private static int serverId;
    private int port;

    private List<User> users = new ArrayList<>();
    private boolean shouldRun = true;

    public Server(int port) {
        this.port = port;

        serverId++;
    }

    @SuppressWarnings("resource")
	public void open() {
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println("Server"+getServerId()+" is listening now !");

            while (shouldRun) {
                Socket socket = serverSocket.accept();
                User user = new User(socket, this);

                user.start();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<User> getUsers() {
        return users;
    }

    public int getServerId() {
        return serverId;
    }

    public void broadcastData(String data) {
        for (User user : getUsers()) {
            user.sendData("[G] " + data);
        }
    }
}
