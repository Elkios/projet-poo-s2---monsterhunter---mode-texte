/**
 * <h1>Handler Class</h1>
 *
 * <p>Classe traduisant les statuts de jeu en actions</p>
 *
 * @author MYYA
 * @version 0.1
 */
package fr.maya.monsterhunter.server.network;

import fr.maya.monsterhunter.client.network.DataFormat;
import fr.maya.monsterhunter.server.Main;
import fr.maya.monsterhunter.server.plateau.Position;

public class Handler {

    private User user;
    private Server server;

    public Handler(User user) {
        this.user = user;

        this.server = user.getServer();
    }

    public void action(String data) {
        DataFormat dataFormat = DataFormat.from(data);
        System.out.println(data);
        data = data.substring(5);

        switch (dataFormat) {
            case LOGIN:
                user.setUsername(data);
                server.getUsers().add(user);

                System.out.println("[+] "+user.getUsername());
                user.sendData(DataFormat.MESSAGE.outputData("Vous êtes bien connecté " + data + " !"));
                break;
            case CLIENT_READY:
                int i = 0;

                user.updateReady();

                for(User user : server.getUsers()) {
                    if(user.isReady()) {
                        i++;
                    }
                }

                if(i == 2) {
                    Main.lobby(server);
                }
                break;
            case BROADCAST:
                server.broadcastData(user.getUsername()+" : "+data);
                break;
            case SELECT_POS:
                user.getGame().move(user, new Position(data));

                break;
		default:
			break;
        }

    }

}
